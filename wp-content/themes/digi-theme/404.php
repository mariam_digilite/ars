<?php get_header(); ?>
    <div class="simple-page">
        <div class="container">
            <h1 class='playfair-font text-center'>Oops! Looks like there is nothing here</h1>
            <div class="">
                <a href="<?php echo site_url(); ?>">
                    <span class="">BACK HOME</span>
                </a>
            </div>
            <div class="playfair-font text-center text-404">
                404
            </div>
        </div>
    </div>
<?php get_footer(); ?>
