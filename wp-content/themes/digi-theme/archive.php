<?php get_header();
if ( get_query_var('paged') ) {
    $paged = get_query_var('paged');
} elseif ( get_query_var('page') ) {
    $paged = get_query_var('page');
} else {
    $paged = 1;
}
$posts_per_page = 8;

$term_id       =    get_queried_object_id();
$prod_term        =    get_term($term_id,'category');
$current_cat_slug =    $prod_term->slug;

$custom_query_args = array(
    'post_type' => 'post',
    'category_name' => $current_cat_slug,
    'posts_per_page' => $posts_per_page,
    'paged' => $paged,
    'post_status' => 'publish',
    'ignore_sticky_posts' => true,
    'order' => 'DESC', // 'ASC'
    'orderby' => 'date'
);
$custom_query = new WP_Query( $custom_query_args );

$count_posts = wp_count_posts( 'post' );
$published_posts = $count_posts -> publish;
$page_count = ceil( $published_posts / $posts_per_page );

if ( $custom_query->have_posts() ) : ?>
<div class="main-content">
    <div class="container">
        <h1 class="p-title playfair-font"><?php echo single_term_title(); ?></h1>

        <?php if($current_cat_slug === 'echo-newsletter'): ?>
            <h2 class="text-right playfair-font newsletter-section-title">Archive Issues:</h2>
            <div class="flex-content newsletter-section">
                <div class="newsletter-sample">
                    <div class="newsletter-sample-img"></div>
                </div>
                <div class="all-newsletters flex-content">
                    <?php while( $custom_query->have_posts() ) : $custom_query->the_post(); ?>
                        <div class="newsletter-box">
                            <?php if(!empty($newsletter_file = get_field('newsletter_file'))): ?>
                                <a class="absolute" href="<?= $newsletter_file; ?>" download></a>
                            <?php endif; ?>
                            <img class="file-icon" src="<?php echo get_template_directory_uri(); ?>/img/pdf_inactive.svg" />
                            <h4 class="newsletter-title text-center"><?= get_the_title(); ?></h4>
                            <?php if(!empty($newsletter_subtitle = get_field('newsletter_subtitle'))): ?>
                                <a class="absolute" href="<?= $newsletter_file; ?>" download></a>
                                <h5 class="newsletter-subtitle text-center"><?= $newsletter_subtitle; ?></h5>
                            <?php endif; ?>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>

        <?php else: ?>

            <div class="row">
                <?php $i = 0; while( $custom_query->have_posts() ) : $i++; $custom_query->the_post(); ?>
                    <div class="col-sm-3">
                        <div class="news-item">
                            <a href="<?= get_the_permalink(); ?>" class="absolute"></a>
                            <div class="img-with-line-grad">
                                <div class="news-img" style="background:url('<?= get_the_post_thumbnail_url(); ?>')  no-repeat top / cover;"></div>
                            </div>
                            <?php $holidays_date = get_field('holidays_date');
                            if(!empty($holidays_date)): ?>
                                <span class="date"><?= $holidays_date; ?></span>
                            <?php else: ?>
                                <span class="date"><?= get_the_date(); ?></span>
                            <?php endif; ?>
                            <h2 class="news-title playfair-font"><?= get_the_title(); ?></h2>
                            <div class="news-content"><?= get_the_content(); ?></div>
                        </div>
                    </div>
                    <?php if($i%4===0): ?>
                        <div class="clearfix"></div>
                    <?php endif; ?>
                <?php endwhile; ?>
            </div>

        <?php endif; ?>

        <?php if ($custom_query->max_num_pages > 1) :
            $orig_query = $wp_query;
            $wp_query = $custom_query;
            ?>
            <div class="pagination">
                <?php
                echo paginate_links( array(
                    'format'  => 'page/%#%',
                    'current' => $paged,
                    'total'   => $custom_query->max_num_pages,
                    'mid_size'        => 2,
                    'prev_text'       => __('&laquo; Prev Page'),
                    'next_text'       => __('Next Page &raquo;')
                ) );
                ?>
            </div>

            <?php $wp_query = $orig_query; ?>
        <?php endif; ?>

        <?php
        wp_reset_postdata(); // reset the query
        endif;
        ?>
    </div>
</div>

<?php get_footer(); ?>

