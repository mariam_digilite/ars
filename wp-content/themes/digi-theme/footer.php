
</main>
        <footer>
            <div class="footer-top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2 col-sm-4">
                            <a class="footer-logo" href="<?php bloginfo("url"); ?>">
                                <svg width="139px" height="128px" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     viewBox="0 0 166.67 130.67" style="enable-background:new 0 0 166.67 130.67;" xml:space="preserve">
                                    <style type="text/css">
                                        .st0{fill:#7B7774;}
                                        .st1{font-family:'PlayfairDisplaySC-Regular';}
                                        .st2{font-size:18px;}
                                    </style>
                                    <title>CB3DD13D-9A33-43E6-9144-F0806EB561AB</title>
                                    <desc>Created with sketchtool.</desc>
                                    <g id="Page-1">
                                        <g id="Home" transform="translate(-135.000000, -3181.000000)">
                                            <g id="Footer" transform="translate(0.000000, 3158.000000)">
                                                <g id="Group-3" transform="translate(135.000000, 23.000000)">

                                                    <image style="overflow:visible;enable-background:new    ;" width="111" height="102" id="ARS_Logo" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG8AAABnCAYAAAAOlhwNAAAACXBIWXMAAA2vAAANrwFCv+cqAAAA
GXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAScxJREFUeNrsvQecHdWVJn7q5dw5
Z3UrZyQhIREkwCQbME7AYAYY57WxmbVnPV6PBzzjXQ/7t9d4nceeBcbM2IxtTDICLJDIEkjQCq3Y
klrqnF+/nOt/vnOrXr8OSjZm7Nmp36/6per3qu53z3e+c+65t4j+c/uT3bT/EFdx1+ZmylCzuiJt
BWl68azH6VqQdL1dntuoi+67uus/wXvHQNpaTJnURrLkVjAQy0lnwDRa8Xt9p07t/B1dDPhuytE2
srna6b5Nwf8E7+0CS9Mv4Vfv5b35tMdnM0Sx6Om/0+EicjrPBtBt3EFe4Fcv03evHPlP8M4WsGyC
gdKuNwCbHaDQBFEySZRKEEWj6v1ZNo8jR7GUZfbfChRNgonn/qLZjorwDiAf48dH/5iA/OMB765n
V1A29zkDsOIpYAGo8TEGLXJmy5q2rWqI065u99n/g8ergCwpmw1MAPkkt9r3yKLvZJ+Z+H8bvDs3
386ncRs/2zgFMIA1Pqr2U2zvXTEoj5+7vIuCcTvd8L3zpnxut+p02dwIPX3Qn3+v2JOmb914gB58
tZ62HSo9/blZbQxgQAFZwsfa7IWfvszi50cC5nev/nfxkbZ/Z9DunuLHwmxhw0NEI4NTDm0ui1Nz
eZzau/0M2BDdtr6HNn1jLX3rpgP06FtV1DXqptvX9874iQpvhmLpqZR51+Un5Fh857ZDa6d8hveD
MRttO1zKj3bViYJjaj9hVQBW1hB5fUQWy4WsbC/kf2unz2z+tqLUdxbEdx68z2xmC9O+xc8mVeII
A9Z7kn3YTBZCg95/xx5pTID01cfbaOP8MbGg9u6APMKKTPDwiONgVTVFHD/U19GLR4N5q/vcZV3y
Gb5j+nb3dUeo2J2R43DMHfcvo4GQkxLoALks0eiw2mGNALG4BNbJoYl2PwiAPvv0l5hOt71TdGp5
R2OxO5/+NV/o1ry8B2i7dxIdOzwDuPFvbxEgYGWPtlexpZ1PKxpCYoHYVjSEaTeDh8dCSrx+5SDd
fe0RKnJn6crVteT1uKdYnQk6tkIAxbp5v+P+pdTy1xvls9s39FJ1UXLmtYRDREcPER3cq65BiSUO
X2gzZbWf0mfZf9+11fYfA7w7n76LL+qtvHoEPc4CGsACaNLzmbrgy2BFAA0NjuewQOxoXFgHPjN9
F8ABADiuNpCh5YsW0tDo2BSrQ0fA/5jfYW7m819/+k06/g/b5Pmjb1XSSNghz9sqY0LTP/3obnrP
siHyObNK5eIaAOIEW3cuh0M/QHqOQUx+lFmm+A/ZrNY/uOxfc/NmfvZJ3l0i7Y8fIerumiLt3/rb
V5jiUgLQJy85SYcGfLTjWDGD0UWPtVeLMHmMG/1Lj8yXY9a1BummNf101ZJhBiVDKxrD9PAbtXTJ
vFE6OOClb29pplI2uPe/az1t27GHDg9qtKBaqVRQIWh2QU1EQHzvyiFy23N0yfxR0W/4jfey9cKn
budzSGVV//7Ld3XRNUuHBbQ/W9tPR4c9dGzEQ5kca750mumULTCV4qvkH7bZfMww7+G9jdb9+RG6
4C9GaPuDuT8dn/eZZ97Lve/+vOwf6FN+bZZ4DEIEVLe8oUosAxa38u82iAVhM63EpDtQG4QLrAmC
BZs6ti0f20Flkp6h8iK8n5T/bX84kP9NgIj/B+1CpIBu0UEeeLVOfGyhRWMDWD5nhn6zu5LWto5T
uS9NLgY9USiIILQmxonqG4lKy+EPP8DWyC4i9RUOhZ6k+66I/PFb3mc338N/fyDWBrAOdxANDXBj
5oTW7nrXCWkwiAE0FIBAQ6CnP/x6jVjciVGPAAarw3HP7KvIg5dIW+U5jkeIEEw6VaDtcIgSbCtP
0aUrGmhuUz2HiCP0fI9DWQR2Cze21SoUl+DgHaDj+x9+oyYP1gR/ZxOf5z+8/zC1sI/9xCXdHBUQ
nRxzs9X18TnW0j+/VktjUfvMa4ewgTqFO4AqtdnwpVfxtdtp7Yf30+v/8rYBqL3tNJlNQknenvdt
hw/krQ29eesXXs/7IFgXKBNWACEC64NYgOUBQDTmV5+YK5Yp0t3c/NwodgbEYQBmmeq6L/Z20n+7
6VLqHxyS1x/7TQIpr1kaOqeoLp1U2ZpIdEYc2Tnkpfev7qfLF47Sr3ZV041M1x19fvrab1qpa+QM
wb/LRdTYqoJ+nKOmP8jv3kP/5+1JiNveVuAyyUklCZo8eSwPFCQ4/At6OpQjQLv7uk55/TnxbVX5
Y//y4YWyT8oqvnCfR2U/vLy7+LRhCinu5bmZqJRbI5RNJ+jIiR5atWwpWxrTWWYWl4Pv9fL3OjmI
z/L3RBMKTGRxYjGhcKHkZ1sok9Xo05tOio8DVQ9MzMyP+lwZEUXRpI1+zpY8wH2XOg8ygBzKllWA
RpGMqGIB95f0nasO/nFY3nTgjh2ZEmgrHxKWGA1qDsDAqmBNAAvWaIJ235aC3LOHG9bnV4/5BtcU
eHZu+GR2BoBF1jh9ZEGEFjVW0M6DJ2nD+g106y/7+dhZcp9W/g4nU6iD90RG7VOSYcxw8RjvcVNJ
nnL7wKoB+vK7j9KSurDQ+aceWkL7en2TB9Q2MGy1fN7CIE9DA/2+AGpvK3CgxxPH88CZ8hv+DQA9
2l4pgCEkKBQkM7uwTwXAtlmIwcqn7LaTxg2uZ3MKQABj4LfM1Ut3XTWfXm/fQw21dXSIe/8/d/nh
KJV1Te8ETsPtm+DNRq8Zfj8aUfFdJjvlo+pAUhgE6nf78SJ+nRIW+eELDWKd+Bx+WYRNOVtyXaM5
qtHOavROsji2032bMu98nDcduAN7p1jc9ewzfv1f3swrRIAGMRCM28QaZ2b5uZHnzeELrGFrsJ+i
u6n+ptk0sqDh7YblGN2w1jbB9GVnBkzTvsNMy7293Ak0ZWWFwDlt8n8Wp4U0vNa0/HfP6N4u9qs1
rB5bmrhTFSvBY2xfvPoYrW6ekI6IWHBBdYQO9nulk3560wna+levC7ASF6Jtujq5k8SNoF7/MmuE
tndebc4GnJHxh9j4wYc76I4Hloqfw/7VJ9pEUX6SlRvU3b2b53BM5pt07NUMWBGrSaddgWExGlKf
Zgp4nz/XHBaygjr5ta4bxzJ9XjcXEUKC2clOg6NBmrB6qddWoawOu6b+HxZncVvZDVloXRmfekKn
eCI31fKM3xILtVnVZxhCgmoFjWazrILL6ccvNtCPXmiUEKWa49V/2VHLcWSU/vbao2JxCOoH+Zoh
flJRFkaJhHIHNnubxDdrP7yLXn9o5J0Db/XNr+V9HEKByGSaCieMbAdAg8XhOSgUqSYA96mHFtO2
w2VKMFRUqoQvnqNxuDE1Axyl0DQFoNmo/Lkcw8B5PDb5PCcAa9RkGaU7LllAr77VQSV+D7m4IxzO
+GnEWsQUqyvfCAt0WVn3WOnmeRn63xcFyaXp9OQxqwIXx6AvoGOwBcNC8VsEl5fOqmNgeRBOGHWA
wDH8IdQ0njltuvi/+pIE3f3YXBpjK7x84QjtOlEk1y9hBAJ7FUoAwGoG8C0GcOwPD96dT9/PF3hV
XpxMG7ZJZCzC+yZokPsIBXDidzywjCnGo6ytplZJ/emWxeBYuYFBjTrAM0OBnOHzQHcMrp0b1c17
2sC2hsZpfYOL+kYm2CgydHJglIKOEhqzF3P7GpbHFndho06/urGXPnJBF9n5Nz7yVAUrxNykIgWl
srVZ3DahVSGAdE6BV6huce5QwNms7AeYLsHiX7zqGB3jMGJ/v0+yMetagpKN2by3ki9N55+xUCYa
V53AjU5gXcS/EGcA951LHHju4KmhnHvkOTImg30zYiOIkxeQuV8wJmkppJugvpC2QoBN5aVKOs/m
YyzKP2l2jdnJlgdQz/slZRV2BtfFjzVMfSloCt7nZPppYXWAv76EDh47ye2ZI3dlLXXrXtYZutDr
314Uo3/80EGqt5/g77LSd1+aS09125hqddU5GDSNAbayVTv5uzUYHX82Cd4pwg1+TMWztK/bS0eG
fEKjc7jDhhJWemJ3FX37uSa6cskIPXDHXhpnSzw86KNUMKxEmRv/b72Qf+wQXfDnh2n7T1Nvv2DB
aDfJ8MfkMM60DQllBNUICRAOIEeIIFyyF6CsFpbMtUyVHkPp4b1CDIW64E8UWzr5GKdXNabmUSJD
M/yhjR/BonUeK/m5odfPZaAGR2nJ3GZuE6VUy5g+PQyyjfdvXDVO91yzj1yDzBbxBPuhanqBrUUz
xYrLKtZm99m4PW38HRbF2OIvp/lDzRBBsFIvU2Edd8aaShFaT7MfhNJEOm004hCVfRuLtb8xqBSK
1GU3VGtfD8vxcZWZ0fW/Z8ted7YjErZzEijZ5K/lOYTJiWP5kQCMgyHbcMP3zxOVBcBmDYirWJTY
nQKWBhHAFCQ9PmNQFhrIoDc0GCynmI9zcQON2nKUZMGT5tAA/2Nh4OAWfWyZdqZSfI1HT7IgrKfO
E73kdbspGoszHhoVsxV/fkOQ/uvFh0nrPCR+Sm9soZMniqkjgs6TEwu3ckdx8O7nL/bw6yG2JKHb
nGGVunEdoG74QavyvTo+AHdDyJSyLx8fl994pqNC/OC3bjxIS2rDEss+3VFOL3eWcFw4yI/FItoy
fSdV+OALVGNYkdt5gB/3vX20uebmr0uODtshFiippOQpt/7VDvFpVzElgCJhE3kVWegboCYRoOom
NXJjuZXvksawqcbIq0xNNZDG71ew3ynl3cKfZyyKQgFegK2whN8vcWrkSwbJEeyj6y5bTz/5xZO0
eP5c6h0YpJqmZqorztLfX9lB1k4OT/q5p/uKKF1cT28cb6Qn+pwcJupkY9C8fD7VvJe5LJRgQwin
cooyU4oJBDAIGFgofKIwBzhbV/EmOiBEDHcctE9nv1Ou1wwlyv1pEVZQ3Ajq51TE+ByKaSRoUQJG
KVAOBPXw2fi/s6NNGf2mu+Q5Ul7TioBgdUh/IYsCukRvmwGcGXDnjLQW+xCdeyvozO23kzNgJ6uP
g2+fPR84w/rgq1L8P1VuCzUzfTYypRV7FaUBd+xobIDY3FDPUUtGgIvG1aBtS0WA/vri42Tv5Q63
/w0yuS+TtVNPzCb/72YwKvl7W3lv8lnFWhP8u2BvnIOOLonQwmsnm59plc/Xxcda2aJ1ATc7Vczg
WssrxJpePlpKf/WLBSLYELAj3rNZdQkbHnylnnrGXep/MBoxxtFCRgD+NPuGMw7oWs+KLvXsZhna
QaK562g+lkMsg3G0r7//kOTyENfddH6/0IVkT0AxAM467WdwkRZTiCDHbOGLYspyqDAA1qgbYsbC
Deni94r5vTIGyG8HXWpiBPB55fwewCtOjtOGpfPo6PEuWjhvLr25d78A+L4NlbS05TDRFmb8KKPh
dorlZUpq2NWU09YhF5Xyd9ey36z3qvMMc6caSeqUYIvT+V8s/Pt2Q8AUiS/UBKsMW1sOKhUZnqw+
0004VFgwFrLQeNROrx0toUW1EYqlrPSJny4hnytL/3T7XkqyiJMYcCKqSiycyApQM7f7Dg4fhn53
y8sl7soXCSEsyJccZKQKC2UKX318rvD58a+/ID4PgkUosL6OT8ShqHC6sDR6K6wvyQ2QYUChHJsD
Nqrhnl1cxNbIDQrBACtI8o54uclvpdaAVSzEY1WCBUmCEwf20qolC+mV9v20sKU+P4LeWM0dboB9
ygT/1qhNZTcSMXJqEaositM8nybf18LfW+VRzWH+njA3W2Uxs0I1WxysvsShifGm+dxzoNOUEftN
D+6xe9iqKsqEfX65q5oW10UYJI/EuRid+MEtHUaW5iitax0nm8adoK/bzMCguOny043GW89Yd5LT
fp0PC4x4DqktZEwwBnbT+QMy+g3QnmaLg/XJ8E0DCnT8KkOBFjYfC/2aiajpxxiMGqbHCrakIhEN
FnkPbeSzweos7Oc0quRG9vD3OOE3uYWdyQmyRMaovrqC+vr6OFTg67XYxedduKyNKsZ+ywJrmD0J
OlKWtICL9wC5y/2UmAhQ1uKmEv7NUEqnce5II4mc6I8K7jw1bI11XvhCi7i9If48zA4xw8foSQM4
zUge2JUalt1M23kMWkwk6Y1OP/dXC33zQwdoGbsYqNFvPtsivnBeZVRqcsbGc8pnuoW5UNz0HFtf
z7lbXlZK89RY10BfvlAHg6ewMjxHglnVhaiCIAEOSWWHW6lHbmAL+yiLD7udLH4HadgDDuXbkCFh
/5dmhCZSoKtcXv7PL7LRfLbEeWwZGTbBhEFNpQxeY7GFmgJKXJT73eLvhsZCEiZsfnEHtdRVTRm3
09MWJhEGoJtbOhpk/zJKZe5+unphLy1i6W6DYjVEr4+peQF/90L+/QW8V7rUZyFGNAaW4F1UMjoi
YlG/U67Hwowh14hrRWgDIYZz9vgkmB+IuPMj73/584X0wKv1kv/FAPQuVr5SUmGGD8pns/rU38ux
dfW5WR6sTtceUCLlOF+wSn8hQ/4My1044Huu65T6EYAIAO99eo7KnMBZmxKbA2M4fEhxO2I2KEzE
XU6VFEYmRZcwCzlKdv7sKxz8HJZX41VCBBYHy0N7ufl7/KwuK3z8mUFz1uFeikQjkhKb31RLv311
F4uWNtp7qJMuXdNAFbb9RJ19lJuwUSbIIsmb5gbnTmZzUElpgor4N44O+2kwaZW2hv+r5r2SrQ0G
hE41xD5wMJaVTAxDR1Y+Pxufu43Bs/H/49ocLpXk1hGyJQ3LxA7l43SJ/9vf7ebgPSb5zn96qYF6
g27acrBM4sJPX3qCusfcNBREbteqsjdWpM+0Z2azvlODt+ZWVVspRUOHp1RZbf7cTsmWQJQAOBT9
oHAnwQqOqqon01m6IU70yfQkEsFOvnA0jp8v2MPU4gGoAJMv3Ex1OSBUeK9mAKv9oFGNg21NOoOL
H8v4/UoGMBseo+OdJ5gih+iTN99AT217hfxMVQNshTHuvRctr6IKdyeHN8coO84WHNYhxMlWk+KT
YQY43M6B/ATV13ro2HAp+24HVbM/reTvjzJoYwAtrtMA02SMe4+bz9fLoAW4E/rhDxkwrwOUqrHw
YAZhSs0lDBFTONYoDtQuLLa7yyt53zgLl319frrzsi76h/cdpvOaQjJaj3ILGUfECLzT5eOvHp0t
dLCeldVxaACQUMf4CfZvD++slYwB0l4YQL336VaV9kKC2T1tjA7nbmQnkJ6SWBdUyXuJofKg9kzV
iOduBi3OJmDXlLIETZZzY1bwDgCtEhoysAG2UkrQ9p2H5KfmzWmmwcEBFill9OLOfVRXXUWt1Taq
L+khrb+XMgNMeRO8hyFE0mQpYuvoniBt8CQzfZwWtDooFCmTREIwDkvTaTShU5BB9LPFwxcjZKlk
0MpdFhFMYIMgAxxl0JBAyMWzk4PE00dEEEJksxSJ6PRaZxGtaQnS1244IhmXAIdaD75aR0/uqaRQ
wqgUQG4un7yeaX3Ws7U69BQM84AuXzhcRjf/4wp5D+NzkvoCXZaVzxx7M3OS0gN1kd54ilxkQrQK
W5FLAdnoVz4MOxrKYoQLcB0Bft1QYhGL87IVwkch6e/S0tTdG5KwwMsXeumaxXSif4yGx4I0ODJG
S1qLqK2iT4Jm/ViQ0uMcO05w54mw0KmKkB5i8XGcYzbbIPlZODS12ujIQDl1R20cpOvK+rmDVbjB
FhYBDecFKxwRq8wqAYNsTELFr/kE92y5W9TecLsifDi/ZUJEn8OWE/CQA/3UxpM0vypGhzl0iET4
e4pLEXIwgvoM67OeIq77oVR+GVZnbojpzBgPVoiMCuhTMiq1TJce51SlZT43X9usxhCPMkj02rgY
pbImJwNdzBbYWMS0xI/l3FhF3FiIqZACc3Lvr2Jray7j993Kl0ZDGZqIpKmyvIw6Dh+hSy9YQ3Fu
nGg0SkPjIVq1qIbaKntVT+4epFS/TulRjbJRFWZY3Ww1+yyS7beUpMnNIUplhZ0ODVVQCQflZQxa
GZ+Hly0+a1gZ1OgAnzjSZ7C4rJGJyQ9ZIQgtvO7prw31+cqhIjkcGZfm8gRdx+KlJ+iiH7MvxChE
Dp0AYKuRB25kUZ4Dp85tqrlxxWqmjgoNEM8BMFVjGZDcpdRRnvSrIh2MgHvdChxuZKgsjAqQYVmS
97UaI+DG68k8tE49LAQSBUEuLgj+DHQJKzM7Mh4nmM7K/RrVlWp07OQIHewMUVd3D/343nvof3//
h7Rj32Fau3iOKM99R7tpaAIJZw5bLCNkmVdKlo4hFhIMRFCjyF4+dauVErg0jXt/a4i0yhGqa+un
VRWNFLJV0jhTZ5DBGkuBRgFYjnr5fMcZsDRTCAJ2qwztqJhUN6hSzymWERGG1xmVr5UeG2AcwlEp
r3j/qgFJViP++9Kv5jN4TokBMSr/9L4KCo4Nq1lKdjvChtV017OdZv3nTPB07XMSt2CKlVGyB+Aw
MmBWd4Emp0yPChSr+g9rTnqZDhBJDetgtBvjbuh8PrsSMqhKsBkAIgHsMOK+NF8cYq1UNpcHzMs9
tdSjCWDmls6aUwZCYm1IQPcPjdG71q+iBx/fSgsbyqmmqpIqy0opmULOkM/d0kFalY/sVSNELD6z
bH2JKKtdN6jOQrE9VnIvTZFjXoIwyFTkiFE/+ztQ5yDT1wADh1wnoAClw0cjkE8ySDkDEzNWj/AL
XEsmo0IgDCdJfjRt7DgQvowZAn4O6r3YnaZb1vWyCh2mkYiDdYRRdIVRd7CfxH3WK/l/UYvfORM8
CJWsWbo3OWUKsRtGDpBJmVLdZRYLwRHrNDkyYEUcpEBM8YVkGLQMyhbg31jmB+zKrwFAxFQ2Azx7
QfwO/CEabJacdATQZE2xRj7UzjrUQeFoKj96gIKjTWuXs88bpJ0HjtP6VcuogqlUlVmw5fk5jAmO
kGO+g6ztSUqx69dj7Lv2ssot4nMcYzCPsAC6LCPXkM5oNBJlwJIq04Lkd5FDhRLm6FAqq2JP7BA1
sMZxI62WSqkMzFTQcpMiBmCw+uwc9or4Q6IDqv3Ony0S0YKirfyGnCfSZi43Bga+bYJnnSZUbpeR
AwiVnsm6UPQMlDAgrsPouNuRm7Q8DIFMr/LKGVdo1I3o0h66ZO8jEGJGEh5gAiz4E2ROaliw1PqV
TK9kuV7Lvg+0iSEzfB0YwY3jmDK9rI8OdI6yr2ul3sFBOny8i6674l0sHMbpSM8oNVeX0sGuPvaH
Y7RpXROfI/fk/p1M33HKdCUp3WuR89JTmqgnncGy+rPk2VRGmco5tLdvDtnZ15Sw1Zey30NIAN+H
rA66DjBBDhRqdJBptY/pdIStMxrPUDpuqM6EoTzT2dlLB6UHJNnXZ+k4+zgIlqGwQ9oawjBfdYYR
B1CnE7lGOk7n37qffV/MNu3LblOUOZqnSPg7ZFRk1gz7N2RRmspikyMGLtfscaJpiUaAp/PJZzPs
X7gHDvMJBdl5B43QAEEwejZ6cLlb+TrkEEGXHgdEiiZ1SdiRgYpx3zrRo+p1jvf00oeuvZruf/gR
enrby7R6QQNtWG6jR7e8THNbW+nNPUZZiL+Rz7WCQWTra2FregNJU2XBubhWUMfpoeFwGVVUFJOT
AU3yDgaABY4xOMP8fDQOH5hj4NRjmAPyJEQL9kIrw2MuN3s5oWl94ZAM3iJHjIpxFGmZG8b98vWf
GBTA8Tb7cmOpkhHbrJRplO+ZNSgoIkJPQGWzmRJTlOmfvYzBKGUQ5WFRDk5EjEWTYRSMy4GK4Cdi
SDgzyBrHNDn28HF+M8Y9OsKAhrhxq/wWoU1QZgn/XHlAkxxAx+ER7uUp6jh0lP7u83dSJ1vew09s
puULP0taNsEdFVmezMx5BJkk2UqR8VFFS45a9rF9KnHuXsS0WFFHBwbmUMxho2GOxwDccEQBBCsL
MSBIk4UYqChcghGEQ7hkdYsqm0DpRgb5XIN9MkZ1QD7rZGp9qwKEhQtCL4yJolwQPg8FS5GkVSq1
83MCMXnFZkfCGjFZ5yR4WWNOOCjTCA8wZQrTocyyb5R5m3PX8v4uD5Y2CRbAsSmwLFBjKCiSoR1N
0luIneDwQUEYCQdl4jkefQ6VQSlhCwy4lD+EwgbzROIqGvEZeYDXdw/L49ZXd9At119DuWSE/vFn
v6JPfOBKWrfUR6+8tZ88ZtIgzWwxdEgeM4O6WJv3Qiv5VyYosoPPz88x3eWN1BFdT77yasrGoOp1
DlVwOcwACQxboVNp0rESWYuAGMXO4EykVQYGnQ/uAeOQWe6UOaMqAKMneQALwUT9SiwmwTvUe7kv
Jamzeze30n3PNU2W1ceUOpV8p0bNbGztBbSpXZKfHGJsCMbNacAzpgKj9M3tUBl108qAo90EDv7Y
Kj0SeFYyWAANyhIBL8QKlGaZBL0koIEu8Rx5y2K3iv3qODD3OBRdNlRo8mhuyWRGZr5ufe112rR+
LW1YuYh+9Mtn6cVd+4U+xyZCFCgyfPPIYUrvHaf4bqLoTg7KKz1U/MV5ZEkOU/HcII365lLf8luo
vqyBhoJEmBnm53NDqJBklzPOFjjBdFnMnQjvxRisIifCCF2yQaVsIUilQcQAzPG0LnP7svw6DbVp
18UaJXaD1cMyASB8ccgufg2TNxEi3PmzxfTQ9tqpoiWdUikzj1GslNVeLvR5yvJCk+Ddt6VJrG/r
F3ZMmdemyuNcKn8nWZRs3uJyGWV9oMmsVdWGYLS8hy8AWREM82DkIACRwjsCXgy0erhHj+E5g4ye
jTQYAJS4zqfJnuafqyvX8pZ33pKF1HHkOFPnEabRTprTtoCu2TBIj7/UTnPry2j5grn06pt76OSD
3ZTu6iBbTxNFOkYpk2aR0Oqn4H1KUSezZXy+KbL9/EEaSil/PXjDDTTSuoAjIEWdIT63OIOIUCbO
AMDiIFgAHHw2wgM8B+WnDYvL8nu5rDECYdaEFtbqmBSKnCf7AuQ1nztQLlT5+SuOS0UCdlAq/J9Y
XxFGbKyt4D1bQXFR83TLQ4iAoqL779grpQ5mMjo/CcSsaM4aVV9w0Nrk+BzA1BmsFMBEoSyDF7Mq
vzck6S2LxH7F7KQxegAwWaxRSQZmqQJ0UCYyKxAuRd5JyvS47XS0i+PPG99H937/J/Td+x+ib3zl
i9TWVE8tnT1ifVddvJYe/e0Y7fnNVqob70MemqyYFMShgZUVqtWO09XJiVwrUpHog2lNXGXxozqN
fP6/E7tVBo4B4h1iJcLXCAuD70N4gNcSDjEg2bQCSylsfZIis7lJmpxNvMAQospVffGqo1J1DasD
haIaDz5QxXxx5bfVYgzFCrxMYoVYEILyaRP7IVBk+u+KQVGfYnkIDWy2U6tMs7Q8nTMqrayK9+2g
RYsIBVghgCtlUAAcfGAJ+7hSl5GENqjTz+8VeZRIgeUNBXU6fmKE6dIuRbULWhroRlabECv/xvt7
Nq2jy9aE6IePPE+XctxX5PezEswY2Y6pVQoW9ml2pj4rUxqyUFhmRTMqEa1HOqi88yAFmpuYQt00
GNZk+vMY+0oHM4oziT6qvnAil5Nzkx/IFywVqM4zbcYcQ1SWYYYR2vk7Nx+QbBbqXxD/zfB7pJWr
OG/dh1FIu1HG7EaG8iV9KHWAw0R1GEKET27sFuuT8MDrO0NFqAJNgjQM98jYl5VcHivHS1YpeUCl
FpK9SPwCtLqAVRLQFiOPLXFhEgzAPZ/pHr4HFrj3wEmKJzJMmSOS07z+ysvp6LFOemHHm7R4wQKm
XPjDJB3r7qP6+kbyv/oKFSejLJ5I7VZV5AXLMx9txqPFcEXpJIcuh0dppKGVIp4iOR9QucvICMF/
uw0RZjeqAHJWg3Esk49mLnfGCMP0ehf2Zymm5kp/in7w4f1iMBga+osLe2hlY1iGjoIoUyyvVJZK
9IbNSIk1yWM0OmVOXSF9YmZPPjB3OE8Pms1MxFpEwFhAf3Y19iX+jS8SQysWQ7TASMVvjGXzgtVj
FBrBCmsCDDCHCJW8ozQHID7/Siedv2KZ+LSrNl1CH7v5/RT60QP03Qf+hf7+ro/T6oUheuDJl2h9
RRW1swgZTdkEKAk8wO7InKTzxWSKNjGK7rJxgO+jdIAFB3eawZNj1K1VUJRsSvXnJmuNUAiAeNTL
J+3lsCbM5zxqxzggMizT4j5ztOFU8/wcarQBoQKAw5QBxHzwgb/cVTVVdRqDtCb3KX+XSuSDc6TB
zOEes5QPk+3zs3pOB5pN5TUBnIPpEGV6ks/kY+DYsQf5YizmNDlDheI5KsSQMpNiWa8mwCFkgMoc
Cul0cpSof1ynWiP1BbGCUAFq84bL1tHzO3bTz5/aSleeP59WLZ4rguNXpUvpgGWDyrkVToHWZlL+
lcsC9N9uaqMwMifhHCUYwHQoKwH6BOpX0rpgkDbKMqbnNTG1IQOFzeadweh/WgF4RhCFszX63tYm
qUiAn8vHeIUbsi34UZvumwpewfAP6BEx3T2PXy0AwhKby0qpPWaf6u9M0MyhEM0IyFHWgMRtIktJ
VjQYXYhYJ0cUzNEFjJ5jsgdCCRT6SJoMBT9+TCJRIwpj3IA9wZz0i3nVRTSnvoq2947IoGsHhwr3
/9sjUqsJtQm18SP2dyvmNdCqRXPpiW3byelQDSPgoTIZ6k6S59pUECMxWr+ylg4PZUUkYRjKywZR
xf4XSTHAnuBGH0XAnspJtggV1VNGE4zy+JxJk+ZAusy5MAqMM+auT4JoKE6MLpx2M2cl6VrrVPCM
DcVEZnhgPspsn8fnTo4IFw625gpK1nG+JucXADyln9lULIgaEAcDXsu+DzSKCi3EfT6nRiEoOt5P
sJWta6unS1vrqKlC0XaHu4z+7bEX6Qurl9GB4z1iffd+/8f0d5//LLsDN4cLy1ltdtCHr7lYObnp
pobMBtOUxkBaHSpWzSVTdN38cvrkNRvp6OAoHRkYo33dgwKgJjROUrWGTmgyxlAOMV2OMvGcit+m
z2conJqmT8uu4LVFXJYxan6Wk5QR7+nSmj4lWNZ++J7CukykwrCUBoQKLAQ+DwnSbz/XrGinqHhq
EtqcPmXumQKKMGfX4MKMyRmYe+fw2Ni/sHBhsdLAfq0IZX0MWhRrtYGyojla0VxHH910Hq1prWcK
VTECFOWqpYvo6W0vcdjgpA9dfy1te20HDY+OUTAUpgtWnyeZlrcOHqWAz0PlFRw77TrGsaVLWRv4
V3Y7WZ0O9scMIl+THg7T92+/gBY3V1JDWTEtb6qhlS31lNJttLc/RKOxjFQ2yLxP0ozRD1VzA3bT
jepquXYpg8io7HUhVRa2kdluhaJlUk2eZnaJVY2u2x0DVpn5o+ufzNdmQmluUGt+yVpfHCogyw0g
JcaTCRH+c5/5btQ0QnXmgXOrWhD0owi3Qr8x2JmzO+nu96yha5c3c4+3CiVphq9CML57/yEpWsKM
oI3nr6R1q86jV3a+SYePdVFFRSWV+hwytflI7yitXraEHtm2V4FnAgcaNcAzv3epR6d/uO3CKaft
cdppUW0ZrZ9bT7GcRrv6QtQXSctggS0/34QFF3fKjMGQemG4dE5tpKkFDAzwUBov8/hy2kxhI4Oz
jmorrf6zBfyPtxeCZy4kA+tTlpiQoXop7QNlngt45vxvlwLO6bVJ9RgmjqBjygg1Sgq4RfpiWVrf
VkP/9KE1VMt+L8UxZ5ppIs1OOpNJs0+30/7DR+nZF1+hyzaspd++vIPVp502nL+aqsuKRHm2dxyg
9Wx9KKN4+a2D/H6ARsfH2HqMBoK/QAMlEpQLR0mPRCkXjdHXP3gerWitkkmZaRZu6VSSFWiG9yy5
GanVTZW0vKGCXjgZpI6RmIwmgCXQMR0GgDkzKjDnEer6qUcUZtsM8DCS87UbDnPH0OlHt3bQ//rA
IToy6FXLZVmdJnizT/GSNSeNsCAYq5RHrJty7ssVaPmlMgCcXcr7mE2YRnpThrPP6ZL/ay710sO3
rKI1NW5KxIM0YfRAzLNzOt3kcntk5MHDAqUs4BEWgkh5cuvL8nj+yvPo2ksO0hMv7BS1eevV66mF
rQbWeVnpMXroK2+wlLUrixhjxdbPvqM3TPqqu0i7/K8K2IuZgAGOx6PyCLeB92ysBheWe+mBG9fS
Fx/fTY/t7aUYgwvBpRl1p0i+63yNOPOcZlPIprJnF6hP0RwhqaS+bOGo1LRgdAGlEqiwTiBOMkTO
rOAB+enrUZojC9NXGzr1FJbJNU5KS1xMv15q7x6ndFzVd4Dvm0q8tHFeJd12wRxaU+emUGicRkdD
YmXIxEB82Gwejkw80ngy66ehXjIrc+vLqbu/VMTK//3FYyJW3nXRBuofGpWR9CM9IzS3oZr2Hu+n
VfUNfN5vTKpKVD2FkqTbaknb8PFp7GWRzoJziMUibPVMX5mMABiNhsjt9tO3b1hOFzdX0M/f6KLt
x0coKpkkUpM++bLbqv00EIxTOJwSPSJTd7O5c7JClAMuqY3IPHZZhMiTkdlFBYJldvDMyZHmCrOo
r7htg1odtivqOospmwZwcK5YoWF+Dd3PYmBGJo1pBdQ4MTFKA/1DFGeHjUFbB4PmYEsTi2Pg7AVJ
gYoyxQhHj3bSpnWrpPyhq7uX7v3BT+juz32Krr5oNZ1ka3v21V302ZvfTT979jVqaYkx6gZymNUT
TJM+liS6+dtE7pmLgltYjbrcXmaErDgwUGgsHqNoJEQRR4it3083raygO9Y3Szw3axt+cwttmxhU
cS9MEwImc3YAojQCS4Rge+5AmcR7mIgpOU7LZIgzK3god4BgAViFWRYRLGfCzq4qyERhSeEp0fVL
64SCzBQRBl3hyyLhIIVDQYpHI0yVUen1Lg/AckjjAThYn1Ygo1FUhB3WtXjhArr3S/+V/u5b3xEL
/MFDD9ON12yiy9Yspp898xqNhWLU1tJM1uCTk+c3wY04lmBu+ghpretPfRnsU9xu5fPhB7NYaI47
V5z9UiwS5j1E/kAJ+Vl5o5NZrVOb8sLGMtrXOUIjWLrDHDaDGWZzp0+V8faxB5fQ955vYrDssgjD
lGAd9R/GlLlZwTP9nfkIy8OkybNSlSr9MOXtjQurWQWHpCeDjkBF6MXxaJSSybhMiARwbo+T/aKL
QfPm/ZxlFppGNgWJ6M6jR+mSTa30hY9+mL7xk4ck07J4XpuMLKD49vntb1IllowyB0pwXuNsdcEU
aZd94cz5Yj4HsEOOA3+cO84TezIeF8aIcacLM9X7AkVyvmAI0L2VKX59Wzn98jUOUcZjUzXAWZge
rA4Ve1CckaRNEibf3do4Y70zm9zBI5ucMdO1nbnd9HuoWcmvune6Nbh0mnFy713TxMrPSv39Y3T8
ZA9FGLB0KkVuVpzB0GTpfUNNlQGcW3q8my3PZpt9FaSrNl5Ijz6zhTa/2k5tra3UOncJ/cX7r6Fv
Pfgrybb89cdvpaUcGz5w4CWaP28BJd0V3HDG4nGQiK1XzkqXM9W7JqCANQBeztgBIDphnH1iMqHo
1Ok2fbON3F4fXTCvnMoDbCV9E1Nj4jNsGIzFlGfMmsVwEBZx/chF3bTlQNl08AYsU2694vEape2D
YmkoiMEUJHPt5Xx65hy261c3KksLT9CRo8fox7/YTA889pyMez33RofQ2/6ufu7lLqZIZXXY7adJ
fgf8frrl+qtFuDz4yGax3vNWnk8fed8VUgb4881bRZU2N9TRrn37yWNNK1ZIcuNF+Pyb1p99+IVZ
S+J7vULhOEcHx7omIwDYBPvD0PgojQ7309joIIUnghwZ6bRxUTWV+11n90NQ12zlmPaMeR9P7qmQ
wiQU4mI5rAKHbGZjBmwzJkLICHrzzPrMKYU8ubNWnbC8WCwoYmTcsLTK0gAV+dw0EVE+dXFbMwUC
AWkgWJzLPdXPzWp9mzZysH6AdnWwr/vpw3TXR2+nC9aup90HjtAruw/ThuXzZLHUJ55/mYoWexRl
hTIc1zFFL7r6nDogrAmMIL4vM7nncqxYDeUHes0gHuXdZnNwH09ycF9Jv9zeRSPhs1js3dAEAxMO
yWaBOkd+sUAm9sAa837P6TZEy+TkSlVV5DjLXnKW1ieU6bKxxA6LYjPBqixRlCXVzKDM2ioBDLTj
9njF/50xEuEGBWBLWhvopdfflKEgj9cvY3tIRO/Ye5h9u11+o3/CSDlNoP5xIVFpwzmHrDYWMAAQ
QsopO1Ok3TZrJ0sm4iJu1pvUeQ6WZy7vhYQInr/SWUK3/tPy/Cq/IlbUb3ZObaVpN0hCsIiRhSkV
Y+cAnkmZMQYPPiMUjeUtD9vla5eSy+mg6ooKsTo0PlQb5pMjDYYQ4HQbjv/MHbcIgBArALC5uY3W
Lp0rFgh/ha1e61F5RhYqtPxG+l020/+5TRVshDKzhQoZVtK4Ziw1s3FRzdlRZ1qNFiDGfuLOnVLD
cjsrfiw0gNF1BOr54ThZeEiPKvA0/YVCn2dushItixdMwZ12dmdteeiFCAVkqt+AKuatYvDgM6AK
MY8c1ubx+MjBfg7Fs5/60j0i/T//9/cKIKfbKipr6eM3vy8P4Iuv76KLz18ln42GjM7i5MdQlvSJ
1DlT5mz+DwACOPjAQv83yYA5ETOpJFPnvAoGz3nWlodSyx+90ERtFTH6/z54kN7625fph8bUOkUB
DlWroWvtxqpopGrbPVNLG7B+GCJ7/CPG8/JVZInEWQFX5LZRmGM5UGbhVlkSEIFy5GQ/rV+13PBz
HnryuW2yY4QcVdBmTeaZLLCusZXee/mFYtFQm9XVtfK8u39wMunL4FHRAqbNBvp9NjNdh87m8niN
LNBM+oSISiaYOudWnB11plXaCytpoGpvX5+P7vzXxbTwKxfTPY+3KaVpsU6uqa2ZgsVqbZfgEbSJ
YJMdMwJ1qE3JtFyr7uODOk6kaoKxM4sWRZlpkdEqU0Eq9mKF+fNnt1NtVRm9deAYzZ8zTvFklm66
/t30evteIwVWJ6pxelbldNuy5Wvog+Nj9JNHn6eXdu6hRv5+ZF+KfB5KWktYZQ4zf15Ab8cmSYSc
IWDMPRcRETPJgin29RGqLioT6jzYO3Fq4WIMsKJiGst6uGw5mfvY0euXQD1fPTYZoEfY4HoMeXlF
O935tKHDi2SuwgOv1Kl4j0EznSV8H5LWokRR5evzndbyEvFInjKxrV7YQvObaiiZ06i1uYmWL1pE
HZ0naE6TsobhEUWrSDRvfW2HPEppDWa9etxnFDBLl62k1YeOi/Veu3EdHesfpbFgiDJIC0XTpK28
8W0Bz/R/WBsNeU+lMjl4z8VUJsmgzpgItQQtayymYq/j1OChYg9Wd9Ivy/5vmDsuC+x8+d2dojqv
vm+1qtt0+5RxqVlCkcJQYZsU3voVeEiFAThz0QDEfmaaLF9DeArwVjSXMmXaqX9sQjIRU3wUW5Ev
UEx+3i9vaqVrr7wi/5m58M3rb+0RsJbMa5N47Ww3+D/kO5E6G2XQAByUpzd0gvSiGtJql9DbtcH/
OV3TrC+bFoszSyIQwCMGFOpk0dI5EDqt5aFeE0VHu7ndv/3bJrVemS9FPePuySp1mV2s7wYUk+BB
tOjaRrG8AsFiDsgCNET88H1ijb2npszbLpkrGYhIZCLfE/N0w37Czf5CfIZrsl4D6tLczl+pqrJR
SCvDPSvUa4gZUCHm3WE27B0fep+kygq31rYFkl1BiIAaFwlPMAzUcgW93ZtkU9hXm8Dhms00mkmd
GJmoqS2jJQ3FtK97nCKJaWIPLoWPW1E3IW2LIB1DP19/3yGp2cRaLXml6TFW18VtUXU9aCmI3B+d
cgBvDzJ1Ir+JmwqWfO5yWVofvQELCYjPi8Vmp8zVTdLrCikTkhoxkqkskZkvdPKwNjPxjNumIUf5
g6/fQ5+5/RZ5zxQzWCwHs4LgE2dTooGiElp/3rL8KvtIBqAMQ1twepW5m31SMJ4+dwCRwPYgneeT
TgkhVph9gc9PMvtcsayOqotnYREslsMAYnoz2hYV6nP/5mL6xrMt9O5lQ2w4RhtjcqVKF6JRd+Je
fZOWp/weUmXFMlI7MihjePB3oEwMxpp3v4I/VCO/4an3PDAos6ncS0MD3XnKFB+BnKUA5yevLyDD
LoUbytaxn2ozxQwWxym00tm2RQsX0lMv7xJZD8XpqWSfN+fUKbHvvHiMmko9tLsvRBMMIEBcXldE
F7eWsRXY8jN3T+3/PDIjCJYn1peZpE+hTnYxKmSYhTrjaoYQrA13DEN2JZKwsdIvmzKqI+5MgYdb
hQdnG1V4mff3CMrGHD2syoNbsaAkDffzQen1zz7eLtY4ELUqsy8A4srl9dzjsuKsTe6HmHAaGRTX
aRLOZ7PVVFZQ/5Ca2lVdWT7rMcUl5eR22Gh0IkppprDq5kWzVx0kM/TInn66YkEl+xqrrC6I+XaY
dzAQTtKzB4cEvMYSN7Vyh3TbracE0OFQGRiERSlnQqXQMIoCIZOMUXFZMbVU+mj3iVGKp7KTvs6I
77Du5mcv72KVf0SCcrQ5VgrEqIJsGJ5S7byP/2GiMD1mjArIHYmN1dbViQKwO/91EX3l0XmyLCNU
EO5gVe5PKeCmUecH1jaLxYEuAB4oBDGcx8tU6Q1ITKdp534vDoQPMuJxsjcf9xUXFVFXTy8NDI3M
GItbOLeVRsaDUmrhWviuWYF7av8glbIK/M3BYfrOS11UxcF0XZGLr89Ji6v9tGFOGZ1XXySvB0NJ
iqezp6dPt8oS4VodDpek+QAgxv8sekYS1RWFMR+szvCPe3t99NBrteRxZOnKxSOUzFjop9tr1Xqc
cGXmTaxgYJp1ZDbLg9/7Fp+JTwAcHRaJClO+sG1cBIwaY7LKzCFMPepkRSeqkwFpqw6wYy6hSHBI
MivSI/lH5YJAl5L++t0WlH/PZRupsbZGOsObHfvlvfLiYvrJz34hiwp86s//jIoDk4VRLU1NlEor
HzY9q7KH6fHLTx2gMXaMXrtNFoObU+ah17qC9MjuAXr34gqxuEVVPj42zODZ5fWbPRO0sMpPpR77
rNZnZ8DcbnPMMq2KmdIpqYdBe1w0v4rBc9PJkajSDIkE1QVi9OlNXdK+mMyJsgesSY15CidHXWpO
OlutsQo+fMchstgTM8HDPcDvfBrDzjfJPVEZPHOVOqRqECwiaYo6enOtza885lVzHBjA97PVoeIp
PDEuPQ70KH6OLc7Hfs5m/93pEiHGhjXnSTjxq6eekffmzmmile6FVFtdKQAWbgvaWlWCgQUOeSeD
/H9+o5u+92oXHR2NU4mLwwibzgBaaYLjwB++dIKOj8VpOJyihhIXjbam6dE9AzQWS1NdiZP8bhs9
3N5Hazlu29hWTtVsqdYCf2jJhw/ZvO/LhrGsVUIAbCqvpnk1AdrfM07xsbBkVT59dRctqonQ8wfL
qaYoQRe0jotrAohxLAkGF4M6WeVqtjA79tF9m/TZR9JxD3CdwcMsIK83P/kE6yQvvfui/HQjKCOs
dCtD+piQycdOUuaE9EQAB3EC4BzOU8droLAo+4Gj3CPXt5w6m+JyOimRTNGyhfMFHAC2btVyeX/6
5ufOBJ/YZiQAYG3/a+tReqtvgkYjaar2uqjEaacA+0b4MoiSiVhGXgO8EIuW3vEEjURTNMEW2h2M
c7Cv03A0SVuOjNIPXjtBi9mHXdBUQiuZWufxc3wPEusuI3wQAcOWl2BFCQ1QUlop1PlKRx+d7I6K
21nVFJIVcHE/vmxOo/9xg7pnH0IGuT+f369mBWkaguDfIpcxOZI+o/pG30lZrV0W6qyqU/dE5Q1W
d8u6Prn9CmTtupYJufERCnQRi7R5NaHM6MQwU0RChInHD+CK5Pmp/ByAOzwcpUXsY+BvfvjqCfrw
qjqhqVnzmGxlN1z1LsO32WYFzqSxxtpqSnrK6QuPd9ArJ4IChouD3OYiD5W7nWJ5+B2sMCgr6may
Mi05K/MPdJnpU+5ysHVaKcjnOc4xmtsKms1ww6boxHicXj4xTn7+jnr2ZVfMr6CrF1ZSTcAp/k/R
Z8oYdVclH2vmlFOplqaTECu6Li5oPVvbq53FtPNEUb4gIT/mXlJmUuYrHIsfpe9clY9nZjqg7f+S
ofNvtbMFXiO5zvERcaq4Fw4mut+0ZkD8Jm76kGb6XMgmv6srQNdtWErXrp9LQ4M9wu/IomA+uN9f
kr/HwfQNlvaxX+yhg0NROsE0FopluUefZOsI0qVMSw7bzESAg6kX2RfspwIOHWLb0VEZ53st7Ke3
hrPi1wBYrddNtT4X1XJjV2Fn6sMyjU6bRRSn04o58DYBtZTfR6bIbbzvQlUZA+m2Yfl+OzlQJs/W
goXj+lmd7ukP01MHsX5YguZXBSjAnQOdADQK6sQwUnVZCT2/4whV2I/RqsZxOjLkpZWNIbp+xRB9
cHU/1RUn6ScvNdC+Xj9l3bild7WaUqfRvaRZttOOh9Kntjy1/Zz3L7PGr5bbhvWckJUgsB4IRhlg
hch7YmFwxIAII666cJ5QJnpYIV2eys9Bov94x0nq6I/QRHGOUnGdxoozVME9fXvXBH3+Cfapq+to
ffOZk9IAC7R4dDRGewfCtJcbMc5W4x0ao0TVHKpiiy5iMIqcuF8f79yoJawyy3gPuNX5hZkmEePF
0zmxPHQcLJ2FQlpI+yD7vQm2vCj/Fuakp3jP6mptbOxYgT6SYusMI/wYlHP6zPpGVrN+sb6s0GeU
SkpydMnSKmpbMUJzy7rlXrRP7q6U0na7Nceq00+vM6NBbVJthTnG+rpkVSyO2NQCpNk2ES6bf8Rw
300V1WJ9KBwyE9RQRt+6qUssEZPgd3S30j1LGqTcAZvkLv3Fs/o5NPAPX+uiFzrHZMW8poCHajxO
qvQ6meu9sniqq9dK+3oidPcI91CfgxqKXaIGfY5JomhnsKBwDzBYoEgsVCrLQeZUOYIjMkCpRIrm
lfmotKKIihkkLwOIkX1YRJnfISqyzFCOJ8eTNDCRkCU4pISdLa2qyCGTSoLxDPtDFbwDPKyNBoDl
hmA2NXMI74XZN8JH9oaYTo+OyzyLj6ypZf8bkKAdpRHYb7q0nsJHnfTY9kpaWh+mWy/oo5eOlMjd
v/b2+BVwSFNiV9mun/FpHDeFypksDxuWbfwEO5bqQt9nKk9MgIBTxVB9c9NSKTnoGwlL7hLATfdz
g0wrP93VQ88eGhFh4NTY9zAgJUxNlRxHzWdZvrIxwOrWL418kEHpDiVocDxFA8GkgEnGXABM1MRS
Gh6sbZbWheLKuAME+BENduzNLZSYGKKJrIWW1RVTW0uFrEaL78UiBrhLCm5f43VaZaKINIQsCWmR
eXUyL4bBK8Ntafh4LBgwLpaZYfrNiqVB4MCK/cY9jbACRJg/659I0uHBKO1mYfRC5zgtYXV56Rw/
+RmIEKtwZFsqymvpkWeW0f99pZesWkpuIoygfMv+cokFxC9h+rKaxPq6xHYWfUZW+9TgfefqAQ4b
vkq46zJiPmRdwiGjIFqXSl6skQwRs6FxuWQWMqywAmx1Xg4NzHgOdPZ4xyDt6p4Qv2PV2U+W+UVm
17NwQKOVsxUsqvGx5bmk4TCI21zmZkUbl0aDQFArHuvSqOjtsCC/S1lSiViVVXzT4c5DtHe4TxZv
i2Zs3CH8tGpesViH1ZhXgIa3WjQqzHpV+tmH2czFWTHh0yJCxmrcGQ5AQsykjDmIdgNs83vUgr46
za300OJaLy2o9tIW7qiPtA9QS6mL5pcElIDhdsrmuNPZl1AoHeMOH6XvPN8kbgkKPo1CI0zhYmsl
ixX5sYe4UTrpvqtzZw+eKon6OV/tJ9h0V1BdE9Hh/TJ5G1NvsZTVRfe20kiimn72zSaOY0KSHvKx
1cHP7ekP0a844EUvxHxvr8VO82vcIhQQQ1WzIqtg0AAEajgDmEFkCJTmMhd/ZqeV8QCFmIrMJaJI
V6sR+ZxqRSVQrLoZlAmGRh27hpjadOqP22XdZ3QE7GdMMPP3lXhseaU6PZ2J38DaZ7punayfpVmO
sWHRA2RpHNwZ3fT84RF65sAolZ1XzR2gSMIoCJj1K+ZS+eb9Ah5GyfM1mVCWuKeg8nVbWWFuY+6e
dSzp9Ff13auD9Nln/gef8S9kWhfW8xoekIQ1JC724srlQplj0YzQ5ZGxNG07MiTpJFjCJSyNm0rc
rOwc7LyVcgPNADS7cU8gbdo9M9CQaHBYVnXOMfXmWUT5wHi2BgxFIjScsHJcpom7OJdEnNWinbEg
/Gwye6qDWcUKy301dIA78MGhGC2qZJWMQVzcMoepvKW2hA6eGGHqL0i7oY0RY2uWMf7Bf+XHY3Tf
lblzB0+10NOsAh7kVr5N7jjM1AVZ8mRfNe5pTbe8+zymg5Tk8iYydvZHWbqotVR8hc+hfAx20J3N
WO7wTNtEOEJHuk5Sd38/x0azV6oV+X2yxkoDx3INNZO3HcDwUTilUSLCvus0CR1879DYGHX3DfDv
heU3zQHhhPGbiBNx+9LqinJq5N/Ao91uO6fOAAtcw+YYhtBhynXa/aRnVMc+f2Ubbe+L02AwMVk3
W1ZpZlMe5F774nSFeW7gYUncz26+h79oA5tym9xtmNTi3tVsTRcubWCVl5XwwM5hY4lHWY7Ncm7J
ZzTmkRMnaefeDllm3+Nm6iwtpaMnu6mEnX3v0PC0PgW/xAE2dxAnN3BbcwPNbW6i85bMpzLXVuph
epu+6J/5G/sOHaETDFqGg3IM2qKUIWuOfrMqxDhcbUUFHe/p53MoppO9/bK+WTF3liXz22jpgnl8
TgFhjbPZVAd2iCLFHBzUY2HF3MvOb6OHj8RosDdMeSpRq0Kgmu9hNpT+6Qrz3MDD9n+u7pIbs2v6
B2QhFGP7swtalvtc9hUZ3I2Lf9RG9DsBdqTrBB062iXDN8igtDU3UjyRlMZD/tbn9cioQozfk9qQ
eEKSzuCScDROY5kwBdlyOg4fZ9AdtLTBw/FTkj/T87+Dsvede/czrcb4e/D/GT5nqyQQrAy+ix+L
An7xR272NykGdXBkVKYXVJeXioWPB0O0bftOennnWzS/pVnWPkOy/Gyt0WHcMdrJ34/fWdpQRMtb
yl84MJ7uTudyBdyp/ZL3DvrOFaedoH4OHKBv4daackO+Oy9p/DI/tFkxCnEOYHX39dMg0xOGciKx
mNBRSVGRJJ8HhkdpeGxA7geEskCrZqGTAxz0csxWyscUBXwst1M0FgqJ+kyL5bAFMZhJ7tJhJJy9
EAsJ2hMlBq2DXnl9Ox8fFiuzcyfz+jj2Y0tQqxKqGaywMPizKHeGEMe0AGteS6M0ch+fZ1fvIHlc
TprT2CDnfPj4CTpw9Dh3LDeVl5RQfXWVXAc6mdt1+jpNVBVguCidysQ/fWH9ltFY6rGn9g+HC5zr
MHN+7Ix1NL9PDcfWN46vePSFt96YU5awYQXdSqNEDxesT5stBJ8SnAixNcTYqpTlpLnR0ZgBn1d8
i5vjGvibloZasjvs3PtVxysvKeKGKZNBUvgkWI2XaRWNFI7FKcINnsCUK7bMhCzLH6NDJ4bppy+F
6XNXFZHH4+HwxSMW7GI1h3NFUruELQ30HOHjUc2NYqWGqkrpBGMTYe5g4+K3kJILR6I0GpwwRgwy
Yv1x/k1MmLHiplT8v0jX4XvLSoolfVdIq0rBWuRxaGRM4uS0pTjCnfaDt7171RZ+/5xvdG/7fcDb
9JmfLCjhCHlVq5MuXoZin5ih6GdOpAclym1m+AKxy2I7pBkXqJa3x7RgNHKCwcGy92ASjFDHQWFj
QRY9VhktKCsOiL8DWKXcUHgfnWKCGzjKjQowfHzcIzt2yxyIhroaKmYfFfB7mBJdbMEBsaIUzol/
B50OoAOwEe5g6Fi4NwOGeNIoKNJTYuUAPSNDXexr+XlA96ul93U9P2Eyzufez+AA0CnyTBS1lver
W3eP0bHBnHs8kl56+9ceQyA+9o6ChyqJ8UiKXj+SpVjaTjdc0kINlSpAt9mss2b6Kb96O9SnteDa
tLwO9zC4aiK/mswPinGwX8Fz+JcAAwzBUsHAocfHWckNT6Sp/UiILlhax+rTIpY6r76LmutradHc
VrGyGAfuezpHaOm8Sgp4XAIErDjOjYnOgLoavNaNireUDKjmxM9KPQoflzWAKmQWqd/M5oxOq886
8VVEEZhjPEpP7DhGu49G2RVkrXwsFql9/HcB73enzXV/s4LP9TUyJjq7OGC+dHUD/cV7VtK6pU0z
hoA6u0cpEk8xDRlAaGqJq4HRCFtVZJrEVo69/XB/vly9kILQAIOjUekg+B40HL57PBSj5ppi+sj1
q+jyNXPom/c/Trdft57Ky8rpsRcP0Y8f20XBSIJqyvzSeaSjuO3UXF0klqVPa/XKEi+V+t18vqqT
ZQvKGGsr/FQacMu5mtaXYjeQZbZorSsjn2fq/d9h4bsO9tADv2mn5944SaFYPgSa4Au8lTTrM7T9
ntQ7Y3m6fiPuuWy+TKSy9FJ7H53ojzItzZwCHeGTRSOr9cYmh3oSbDWJ1Ox0H4zEZ33/NP/T2T8W
eWVgLHrlP/56V/XgyDC9evC3MkDaNxyKHOsbe5nPufpoz9iKyYSAZUZD5yU+hyEOdJBZyvrdGAe0
W6d0UmWhJN9ndo7CbTwcp+6BCe5oU0oMi7gx15GeBnUO/uEtb8U9PnJmMJGhmt7+rX067nyWR6d1
HGSpp9X/aQP8QScfO8KquI0ff4CyGmVM+gCzxJf4ve3S4XR9Nbf6NIWst814T6fls5xfGwZO3uZr
BnAfox1f2/OHtzxn+j3owWZvNwpBCxuyE/PHpjVE+1kARWZNYqG7mPn9/J42/T1rgmknYXSuHnJk
P6Zr+u3SPXPaQ8xb2+n1/6n+Z81/72S+nX5PCd8s7THb/VqnHqeTm38DmYvC8S88b+T33QXHueS9
KcfppUY7LuUOtYjP6wi98T/jf1jLW/vlev7XtrxlqAYu/Fq2Fj1zBlCItn8t+LbbbfFV1RR8eoAq
3ltP1fXFFB5ro9B4D41t3pn/7O3cVtxjZRbyQDBP8ic/t5BnSqWCrvFzfep7pDsM1wPHeYj3IQYv
+4cXLH+MW/G7uEPpuM/tN3m/RXUqbTe/936j8/Cu/YiCv+38j3C5FvqPtQ0Y6g20djsqABm4xfz6
QfWcPvAH8Ff/btufruUVb8Ttw9qEoik9oJ7b+DF9uQGe3G+Hn2PvwU1iCoCLUHBLO1vqVYrQf/v0
n2ITWP9kwXO1/Tn/vYPkZh4WzCI5zgBdyUBdyNZ2gB9xw6OXeF/Fr+sYrMfJNedSfo26wWv4+XpD
mq/h5+OUODbwp9YEf8q0Wa9CFR0WZTr5EuP9LTS5aBVq4Juo+LIv8uNiQyniHqwcfuhrlfS3JP4U
G+D/F2AATP28oKH7OJcAAAAASUVORK5CYII=" transform="matrix(0.8104 0 0 0.8104 0 0.0977)">
                                                    </image>
                                                    <text transform="matrix(1 0 0 1 1 109)" class="st0 st1 st2">A.R.S.</text>
                                                    <text transform="matrix(1 0 0 1 47.3315 109)" class="st0 st1 st2"> </text>
                                                    <text transform="matrix(1 0 0 1 1 129)" class="st0 st1 st2">Armenian School</text>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
</svg>
                            </a>
                            <?php get_template_part( 'social'); ?>
                        </div>
                        <div class="col-md-2 col-sm-4">
                            <?php if ( has_nav_menu( 'footer-menu1' )) : ?>
                                <h5 class="playfair-font f-menu-title">Legal Terms</h5>
                                <?php wp_nav_menu( array( 'theme_location' => 'footer-menu1', 'container'=> '','menu_class'=>'footer-menu')); ?>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-2 col-sm-4">
                            <?php if ( has_nav_menu( 'footer-menu2' )) : ?>
                                <h5 class="playfair-font f-menu-title">Useful Links</h5>
                                <?php wp_nav_menu( array( 'theme_location' => 'footer-menu2', 'container'=> '','menu_class'=>'footer-menu')); ?>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-2 col-sm-4">
                            <h5 class="playfair-font f-menu-title">Contact Info</h5>
                            <?php if(get_field('phone_number', 'options')): ?>
                                <a href="tel:"><?php the_field('phone_number', 'options'); ?></a>
                            <?php endif; ?>
                            <?php if(get_field('address', 'options')): ?>
                                <a href="https://www.google.com/maps/place/50+Hallcrown+Pl,+North+York,+ON+M2J+1P6,+Canada/@43.7693295,-79.3262892,17z/data=!3m1!4b1!4m5!3m4!1s0x89d4d25cd55dd301:0x8378e2e068766283!8m2!3d43.7693256!4d-79.3241005" target="_blank">
                                    <?php the_field('address', 'options'); ?>
                                </a>
                            <?php endif; ?>
                            <?php if(get_field('email', 'options') || get_field('another_email', 'options')): ?>
                                <p><a href="mailto:<?php the_field('email', 'options'); ?>">
                                    <?php the_field('email', 'options'); ?>
                                </a></p>
                                <p><a href="mailto:<?php the_field('another_email', 'options'); ?>">
                                        <?php the_field('another_email', 'options'); ?>
                                </a></p>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <h5 class="playfair-font f-menu-title">Get Involved</h5>
                            <?php if(get_field('get_involved', 'options')): ?>
                                <?php the_field('get_involved', 'options'); ?>
                            <?php endif; ?>
                            <a href="https://acc.akaraisin.com/Donation/Event/DonationInfo.aspx?seid=18148&mid=48" target="_blank" class="button">Donate</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container flex-content">
                    <span class="copyright"> Copyright &copy; 1999-<?php echo date("Y"); ?> A.R.S. Armenian School.</span>
                    <span class="dd-team push">designed & developed by <a href="https://digilite.ca/" target="_blank">Digilite Web Solutions</a></span>
                </div>
            </div>
		</footer>
		<?php wp_footer(); ?>
	</body>
</html>