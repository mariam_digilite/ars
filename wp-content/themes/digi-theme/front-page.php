<?php get_header(); ?>
    <section class="section section-dontae">
        <div class="container">
            <div class="donate-section text-center">
                <h1 class="playfair-font">It’s Your Call!</h1>
                <h2 class="playfair-font">Donate to Educate</h2>
                <a href="https://acc.akaraisin.com/Donation/Event/DonationInfo.aspx?seid=18148&mid=48" target="_blank" class="button">Donate Now</a>
                <div class="image"></div>
            </div>
        </div>
    </section>

    <section class="section section-welcome">
        <div class="container">
            <div class="row">
                <div class="astronaut"></div>
                <div class="col-md-6 col-md-offset-3">
                    <?php if(get_field('title')): ?>
                        <h2 class="playfair-font title"><?php the_field('title'); ?></h2>
                    <?php endif; ?>
                    <?php if(get_field('text')): ?>
                        <article><?php the_field('text'); ?></article>
                    <?php endif; ?>
                </div>
                <div class="col-md-3">
                    <div class="image"></div>
                </div>
            </div>
        </div>
    </section>

    <section class="section section-explore">
        <?php if(get_field('explore_section_title')): ?>
            <h2 class="donate-section title playfair-font"><?php the_field('explore_section_title'); ?></h2>
        <?php endif; ?>
        <?php if($sections = get_field('sections')): ?>
            <div class="vertical-slider">
                <div class="blog-slider">
                    <div class="blog-slider__wrp swiper-wrapper">
                        <?php foreach($sections as $section): ?>
                            <div class="blog-slider__item swiper-slide">
                                <div class="blog-slider__img">
                                    <img src="<?= $section['section_image']; ?>" alt="">
                                </div>
                                <div class="blog-slider__content">
                                    <div class="playfair-font blog-slider__title"><?= $section['section_title']; ?></div>
                                    <div class="blog-slider__text"><?= $section['section_text']; ?></div>
                                    <a href="<?= $section['button_link']; ?>" class="button">Read More</a>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="blog-slider__pagination"></div>
                </div>
            </div>
        <?php endif;?>
    </section>

    <section class="section subscribe-section">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="subscribe-form">
                        <h5>Interested in A.R.S. Armenian School updates?</h5>
                        <?php echo do_shortcode('[formidable id=6]'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section news-section">
        <div class="container">
            <h2 class="donate-section title playfair-font">School News</h2>
            <?php
            $args = array(
                'post_type' => 'post',
                'category_name' => "school-news",
                'posts_per_page' => 3,
                'post_status'    => 'publish',
                'order' => 'DESC'
            );
            $query = new WP_Query( $args );
            if ( $query->have_posts() ): ?>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="school-news">
                        <?php $i = 0; while ( $query->have_posts() ) : $query->the_post(); $i ++; ?>
                            <div class="tile post-box col-<?= $i; ?>">
                                <a href="<?php the_permalink(); ?>" class="absolute"></a>
                                <div class="post-img" style="background:url('<?php the_post_thumbnail_url('');  ?>') no-repeat top / cover;"></div>
                                <div class="post-content">
                                    <h3 class="playfair-font post-title"><?php the_title(); ?></h3>
                                    <div class="post-text"><?php the_content(); ?></div>
                                    <a class="read-more text-right" href="<?php the_permalink(); ?>" class="absolute">Read More >></a>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <a href="<?php echo site_url(); ?>/category/school-news/" class="button">Show More News</a>
        </div>
    </section>

    <section class="section mission-section">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-md-offset-2">
                    <div class="mission-content">
                        <p>A.R.S. Day School is committed to providing a balanced and fulfilling learning experience
                            in a secure and nurturing environment that develops academic excellence, leadership,
                            a creative spirit, good sportsmanship and strong communication skills.</p>
                        <h4 class="playfair-font">A.R.S. Armenian School Mission Statement</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>