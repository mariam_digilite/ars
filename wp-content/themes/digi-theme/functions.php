<?php
function blank_widgets_init() {
	// Area 1, located at the top of the sidebar.
	register_sidebar(array('name' => 'Sidebar', 'id' => 'sidebar', 'before_widget' => '<div id="%1$s" class="widget-container %2$s">', 'after_widget' => '</div>', 'before_title' => '<h3 class="widget-title">', 'after_title' => '</h3>', ));
}
add_action("widgets_init", "blank_widgets_init");

/* Add Thumbnail Support */
add_theme_support("post-thumbnails"); 
add_image_size("blog-thumb", 200, 200, true); //(width, height, crop)

/* change e-mail name */
function res_fromemail($email) {
	$wpfrom = get_option("admin_email");
	return $wpfrom;
}
add_filter("wp_mail_from", "res_fromemail");

function res_fromname($email) {
	$wpfrom = get_option("blogname");
	return $wpfrom;
}
add_filter("wp_mail_from_name", "res_fromname");

/* custom read more and excerpt heigh. 
 * usage: echo my_excerpts(20, $post) or echo my_excerpts(20)
 * or use wp_trim_words() @ https://developer.wordpress.org/reference/functions/wp_trim_words/
 */
function my_excerpts($excerpt_length, $content = false) {
	global $post;
	$mycontent = $post->post_excerpt;
	$link = $post->guid;

	$mycontent = __($post->post_content);
	$mycontent = strip_shortcodes($mycontent);
	$mycontent = str_replace("]]>", "]]&gt;", $mycontent);
	$mycontent = strip_tags($mycontent);
	$words = explode(" ", $mycontent, $excerpt_length + 1);
	if(count($words) > $excerpt_length) :
	array_pop($words); 
	array_push($words, "...");
	$mycontent = implode(" ", $words);
	endif;

	// Make sure to return the content
	return $mycontent;
}

function my_scripts_method() {
    wp_enqueue_style("Source-sans", "//fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900&display=swap", array(), "", "all");
    wp_enqueue_style("Playfair-font", "//fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i&display=swap", array(), "", "all");
    wp_enqueue_style("bootstrap-style", "//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css", array(), "", "all");
	wp_enqueue_style("font-awesome", "//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css", array(), "", "all");
    if(is_home() || is_front_page()){
        wp_enqueue_style("swiper", "//cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/css/swiper.min.css", array(), "", "all");
    }
    wp_enqueue_style("matrial-icons", "https://fonts.googleapis.com/icon?family=Material+Icons", array(), "", "all");
    if (is_page('contact-us')) {
        wp_enqueue_script("google-api", "//maps.googleapis.com/maps/api/js?key=AIzaSyAG1EChKTqRIrN8-DmjmlsqUYUB8ByPlFw", "", "", true);
        wp_enqueue_script("map-settings-js", get_template_directory_uri() . "/js/google-maps-settings.js","","",true);
    }
	wp_enqueue_style("stylecss", get_stylesheet_uri());
	wp_enqueue_script("jquery");
	wp_enqueue_script("bootstrap", "//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js", "", "", true);
    if(is_home() || is_front_page()){
        wp_enqueue_script("swiper", "//cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/js/swiper.min.js", "", "", true);
    }
	wp_enqueue_script("functions", get_template_directory_uri() . "/js/functions.js", "", "", true);
}
add_action("wp_enqueue_scripts", "my_scripts_method");

/* WYSIWYG defaults */
/** change tinymce's paste-as-text functionality */
function change_paste_as_text($mceInit, $editor_id) {
	//turn on paste_as_text by default
	//NB this has no effect on the browser's right-click context menu's paste!
	$mceInit["paste_as_text"] = true;
	return $mceInit;
}
add_filter("tiny_mce_before_init", "change_paste_as_text", 1, 2);

/** Set the Attachment Display Settings, This function is attached to the 'after_setup_theme' action hook. */
function default_attachment_display_setting() {
	update_option("image_default_align", "left");
	//update_option("image_default_link_type", "none");
	//update_option("image_default_size", "large");
}
add_action("after_setup_theme", "default_attachment_display_setting");


// Breadcrumbs
function custom_breadcrumbs() {
	// Settings
	$prefix             = "";
	$separator          = "&gt;";
	$breadcrums_id      = "breadcrumbs";
	$breadcrums_class   = "breadcrumbs";
	$home_title         = "Home";
		
	// If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
	$custom_taxonomy    = "product_cat";
		 
	// Get the query & post information
	global $post,$wp_query;
		 
	// Do not display on the homepage
	if(!is_front_page()) {
		 
		// Build the breadcrums
		echo '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';
			 
		// Home page
		echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
		echo '<li class="separator separator-home"> ' . $separator . ' </li>';
			 
		if(is_archive() && !is_tax() && !is_category() && !is_tag()) {
			echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';
		} elseif(is_archive() && is_tax() && !is_category() && !is_tag()) {
			// If post is a custom post type
			$post_type = get_post_type();
				
			// If it is a custom post type display name and link
			if($post_type != "post") {
				$post_type_object = get_post_type_object($post_type);
				$post_type_archive = get_post_type_archive_link($post_type);
				
				echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
				echo '<li class="separator"> ' . $separator . ' </li>';
			}
				
			$custom_tax_name = get_queried_object()->name;
			echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';
				
		} elseif(is_single()) {
			// If post is a custom post type
			$post_type = get_post_type();
				
			// If it is a custom post type display name and link
			if($post_type != "post") {
				$post_type_object = get_post_type_object($post_type);
				$post_type_archive = get_post_type_archive_link($post_type);
				
				echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
				echo '<li class="separator"> ' . $separator . ' </li>';
			}
				
			// Get post category info
			$category = get_the_category();
			 
			if(!empty($category)) {
				// Get last category post is in
				$last_category = end(array_values($category));
					
				// Get parent any categories and create array
				$get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ","), ",");
				$cat_parents = explode(",", $get_cat_parents);
					
				// Loop through parent categories and store in variable $cat_display
				$cat_display = "";
				foreach($cat_parents as $parents) {
					$cat_display .= '<li class="item-cat">'.$parents.'</li>';
					$cat_display .= '<li class="separator"> ' . $separator . ' </li>';
				}
			 
			}
				
			// If it's a custom post type within a custom taxonomy
			$taxonomy_exists = taxonomy_exists($custom_taxonomy);
			if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
				$taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
				$cat_id         = $taxonomy_terms[0]->term_id;
				$cat_nicename   = $taxonomy_terms[0]->slug;
				$cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
				$cat_name       = $taxonomy_terms[0]->name;
			}
				
			// Check if the post is in a category
			if(!empty($last_category)) {
				echo $cat_display;
				echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
			// Else if post is in a custom taxonomy
			} elseif(!empty($cat_id)) {
				echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
				echo '<li class="separator"> ' . $separator . ' </li>';
				echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
			} else {
				echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
			}
				
		} elseif(is_category()) {
			// Category page
			echo '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';
		} elseif(is_page()) {
			// Standard page
			if($post->post_parent) {
				// If child page, get parents 
				$anc = get_post_ancestors( $post->ID );
					 
				// Get parents in the right order
				$anc = array_reverse($anc);
					 
				// Parent page loop
				foreach($anc as $ancestor) {
					$parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
					$parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
				}
					 
				// Display parent pages
				echo $parents;
					 
				// Current page
				echo '<li class="item-current item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';
			} else {
				// Just display current page if not parents
				echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li>';
			}
				 
		} elseif(is_tag()) {
			// Tag page
				 
			// Get tag information
			$term_id        = get_query_var("tag_id");
			$taxonomy       = "post_tag";
			$args           = "include=" . $term_id;
			$terms          = get_terms( $taxonomy, $args );
			$get_term_id    = $terms[0]->term_id;
			$get_term_slug  = $terms[0]->slug;
			$get_term_name  = $terms[0]->name;
				 
			// Display the tag name
			echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';
			 
		} elseif(is_day()) {
			// Day archive
				 
			// Year link
			echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
			echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
				 
			// Month link
			echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
			echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';
				 
			// Day display
			echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';
				 
		} elseif(is_month()) {
			// Month Archive

			// Year link
			echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
			echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
				 
			// Month display
			echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';
				 
		} elseif(is_year()) {
				 
			// Display year archive
			echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';
				 
		} elseif(is_author()) {
			// Auhor archive
				 
			// Get the author information
			global $author;
			$userdata = get_userdata( $author );
				 
			// Display author name
			echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';
			 
		} elseif(get_query_var("paged")) {
			// Paginated archives
			echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';
				 
		} elseif(is_search()) {
			// Search results page
			echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</strong></li>';
			 
		} elseif(is_404()) {
				 
			// 404 page
			echo '<li>Error 404</li>';
		}
		echo '</ul>';
	}
}

//ORDERBY taxonomy term name. usage "orderby" => "my-taxonomy-slug"
function posts_clauses_with_tax($clauses, $wp_query) {
	global $wpdb;
	//array of sortable taxonomies
	$taxonomies = array("my-taxonomy-slug"); // be sure to change "my-taxonomy-slug"
	if(isset($wp_query->query["orderby"]) && in_array($wp_query->query["orderby"], $taxonomies)) {
		$clauses["join"] .= "
			LEFT OUTER JOIN {$wpdb->term_relationships} AS rel2 ON {$wpdb->posts}.ID = rel2.object_id
			LEFT OUTER JOIN {$wpdb->term_taxonomy} AS tax2 ON rel2.term_taxonomy_id = tax2.term_taxonomy_id
			LEFT OUTER JOIN {$wpdb->terms} USING (term_id)
		";
		$clauses["where"] .= " AND (taxonomy = '{$wp_query->query['orderby']}' OR taxonomy IS NULL)";
		$clauses["groupby"] = "rel2.object_id";
		$clauses["orderby"] = "GROUP_CONCAT({$wpdb->terms}.name ORDER BY name ASC) ";
		$clauses["orderby"] .= ("ASC" == strtoupper($wp_query->get("order"))) ? "ASC" : "DESC";
	}
	return $clauses;
}
add_filter("posts_clauses", "posts_clauses_with_tax", 10, 2);

if(function_exists("acf_add_options_page")) {
	acf_add_options_page(array(
		"page_title"    => "Options",
		"menu_title"    => "Theme Settings",
		"menu_slug"     => "theme-general-settings",
		"icon_url"      => "dashicons-images-alt2",
		"capability"    => "edit_posts",
		"redirect"      => false
	));
}

function my_acf_add_local_field_groups() {
	acf_add_local_field_group(array(
		"key" => "settings",
		"title" => "Settings",
		"fields" => array(
			array(
				"key" => "social_links",
				"label" => "Social Links",
				"name" => "social_links",
				"type" => "repeater",
				"sub_fields" => array(
					array(
						"key" => "icon",
						"label" => "Icon",
						"name" => "icon",
						"type" => "font-awesome",
					),
					array(
						"key" => "link",
						"label" => "Link",
						"name" => "link",
						"type" => "url",
					),
				)
			)
		),
		"location" => array(
			array(
				array(
					"param" => "options_page",
					"operator" => "==",
					"value" => "theme-general-settings",
				),
			),
		),
	));
}
add_action("acf/init", "my_acf_add_local_field_groups");

function share_buttons() {
	$link = get_the_permalink();
	$title = get_the_title();
	$img = get_the_post_thumbnail_url();
	$src = get_bloginfo("url");

	$result = "<li><a class=\"transition facebook\" href=\"https://www.facebook.com/sharer/sharer.php?u=".$link."\" target=\"_blank\" ><i class=\"fa fa-facebook-square\"></i></a></li>".
	"<li><a class=\"transition twitter\" href=\"https://twitter.com/home?status=".$link."\" target=\"_blank\"><i class=\"fa fa-twitter-square\"></i></a></li>".
	"<li><a class=\"transition google\" href=\"https://plus.google.com/share?url=".$link."\" target=\"_blank\"><i class=\"fa fa-google-plus-square\"></i></a></li>".
	"<li><a class=\"transition linkedin\" href=\"https://www.linkedin.com/shareArticle?mini=true&url=".$link."&title=".$title."&summary=&source=".$src."\" target=\"_blank\"><i class=\"fa fa-linkedin-square\"></i></a></li>".
	"<li><a class=\"transition pinterest\" href=\"https://pinterest.com/pin/create/button/?url=".$link."&media=".$img."&description=\" target=\"_blank\"><i class=\"fa fa-pinterest-square\"></i></a></li>";
	return $result;
}
add_shortcode("share", "share_buttons");

/**
 * Extended Walker class for use with the
 * Twitter Bootstrap toolkit Dropdown menus in Wordpress.
 * Edited to support n-levels submenu.
 * @author johnmegahan https://gist.github.com/1597994, Emanuele 'Tex' Tessore https://gist.github.com/3765640
 * @license CC BY 4.0 https://creativecommons.org/licenses/by/4.0/
 */
//class BootstrapNavMenuWalker extends Walker_Nav_Menu {
//	function start_lvl(&$output, $depth) {
//		$indent = str_repeat( "\t", $depth );
//		$submenu = ($depth > 0) ? ' sub-menu' : '';
//		$output	   .= "\n$indent<ul class=\"dropdown-menu$submenu depth_$depth\">\n";
//	}
//
//	function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
//		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
//		$li_attributes = '';
//		$class_names = $value = '';
//		$classes = empty($item->classes) ? array() : (array) $item->classes;
//
//		// managing divider: add divider class to an element to get a divider before it.
//		$divider_class_position = array_search('divider', $classes);
//		if($divider_class_position !== false){
//			$output .= "<li class=\"divider\"></li>\n";
//			unset($classes[$divider_class_position]);
//		}
//
//		$classes[] = ($args->has_children) ? 'dropdown' : '';
//		$classes[] = ($item->current || $item->current_item_ancestor) ? 'active' : '';
//		$classes[] = 'menu-item-' . $item->ID;
//		if($depth && $args->has_children){
//			$classes[] = 'dropdown-submenu';
//		}
//		$class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args));
//		$class_names = ' class="' . esc_attr($class_names) . '"';
//		$id = apply_filters('nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args);
//		$id = strlen($id) ? ' id="' . esc_attr($id) . '"' : '';
//		$output .= $indent . '<li' . $id . $value . $class_names . $li_attributes . '>';
//		$attributes  = ! empty($item->attr_title) ? ' title="'  . esc_attr($item->attr_title) .'"' : '';
//		$attributes .= ! empty($item->target)     ? ' target="' . esc_attr($item->target    ) .'"' : '';
//		$attributes .= ! empty($item->xfn)        ? ' rel="'    . esc_attr($item->xfn       ) .'"' : '';
//		$attributes .= ! empty($item->url)        ? ' href="'   . esc_attr($item->url       ) .'"' : '';
//		$attributes .= ($args->has_children) 	    ? ' class="dropdown-toggle" data-toggle="dropdown"' : '';
//		$item_output = $args->before;
//		$item_output .= '<a'. $attributes .'>';
//		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
//		$item_output .= ($depth == 0 && $args->has_children) ? ' <b class="caret"></b></a>' : '</a>';
//		$item_output .= $args->after;
//		$output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
//	}
//
//	function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output) {
//		//v($element);
//		if (!$element)
//			return;
//		$id_field = $this->db_fields['id'];
//		//display this element
//		if ( is_array( $args[0] ) )
//			$args[0]['has_children'] = ! empty($children_elements[$element->$id_field]);
//		else if ( is_object( $args[0] ) )
//			$args[0]->has_children = ! empty($children_elements[$element->$id_field]);
//		$cb_args = array_merge( array(&$output, $element, $depth), $args);
//		call_user_func_array(array(&$this, 'start_el'), $cb_args);
//		$id = $element->$id_field;
//		// descend only when the depth is right and there are childrens for this element
//		if(($max_depth == 0 || $max_depth > $depth + 1) && isset($children_elements[$id])) {
//			foreach($children_elements[ $id ] as $child) {
//				if (!isset($newlevel)) {
//					$newlevel = true;
//					//start the child delimiter
//					$cb_args = array_merge( array(&$output, $depth), $args);
//					call_user_func_array(array(&$this, 'start_lvl'), $cb_args);
//				}
//				$this->display_element($child, $children_elements, $max_depth, $depth + 1, $args, $output);
//			}
//			unset($children_elements[$id]);
//		}
//
//		if (isset($newlevel) && $newlevel) {
//			//end the child delimiter
//			$cb_args = array_merge(array(&$output, $depth), $args);
//			call_user_func_array(array(&$this, 'end_lvl'), $cb_args);
//		}
//
//		//end this element
//		$cb_args = array_merge(array(&$output, $element, $depth), $args);
//		call_user_func_array(array(&$this, 'end_el'), $cb_args);
//	}
//}

/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * It is expected that theme authors would copy and paste this code into their
 * functions.php file, and amend to suit.
 *
 * @see http://tgmpluginactivation.com/configuration/ for detailed documentation.
 *
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.6.1 for parent theme Blank Theme
 * @author     Thomas Griffin, Gary Jones, Juliette Reinders Folmer
 * @copyright  Copyright (c) 2011, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/TGMPA/TGM-Plugin-Activation
 */

/**
 * Include the TGM_Plugin_Activation class.
 *
 * Depending on your implementation, you may want to change the include call:
 *
 * Parent Theme:
 * require_once get_template_directory() . '/path/to/class-tgm-plugin-activation.php';
 *
 * Child Theme:
 * require_once get_stylesheet_directory() . '/path/to/class-tgm-plugin-activation.php';
 *
 * Plugin:
 * require_once dirname( __FILE__ ) . '/path/to/class-tgm-plugin-activation.php';
 */
require_once get_template_directory() . "/class-tgm-plugin-activation.php";

add_action("tgmpa_register", "blank_theme_register_required_plugins");

function blank_theme_register_required_plugins() {
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(

		// This is an example of how to include a plugin bundled with a theme.
		array(
			"name"               => "Advanced Custom Fields Pro", // The plugin name.
			"slug"               => "advanced-custom-fields-pro", // The plugin slug (typically the folder name).
			"source"             => get_template_directory() . "/plugins/advanced-custom-fields-pro.zip", // The plugin source.
			"required"           => true, // If false, the plugin is only 'recommended' instead of required.
			"version"            => "", // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			"force_activation"   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			"force_deactivation" => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			"external_url"       => "", // If set, overrides default API URL and points to an external URL.
			"is_callable"        => "", // If set, this callable will be be checked for availability to determine if a plugin is active.
		),

		array(
			"name"               => "Formidable Pro", // The plugin name.
			"slug"               => "formidable-pro", // The plugin slug (typically the folder name).
			"source"             => get_template_directory() . "/plugins/formidable-pro-2.03.01.zip", // The plugin source.
			"required"           => true, // If false, the plugin is only 'recommended' instead of required.
			"version"            => "", // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			"force_activation"   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			"force_deactivation" => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			"external_url"       => "", // If set, overrides default API URL and points to an external URL.
			"is_callable"        => "", // If set, this callable will be be checked for availability to determine if a plugin is active.
		),

		array(
			"name"               => "Advanced Custom Fields Font Awesome Add-on", // The plugin name.
			"slug"               => "acf-font-awesome-add-on", // The plugin slug (typically the folder name).
			"source"             => get_template_directory() . "/plugins/advanced-custom-fields-font-awesome.zip", // The plugin source.
			"required"           => true, // If false, the plugin is only 'recommended' instead of required.
			"version"            => "", // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			"force_activation"   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			"force_deactivation" => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			"external_url"       => "", // If set, overrides default API URL and points to an external URL.
			"is_callable"        => "", // If set, this callable will be be checked for availability to determine if a plugin is active.
		),

	);

	/*
	 * Array of configuration settings. Amend each line as needed.
	 *
	 * TGMPA will start providing localized text strings soon. If you already have translations of our standard
	 * strings available, please help us make TGMPA even better by giving us access to these translations or by
	 * sending in a pull-request with .po file(s) with the translations.
	 *
	 * Only uncomment the strings in the config array if you want to customize the strings.
	 */
	$config = array(
		"id"           => "blank-theme",                 // Unique ID for hashing notices for multiple instances of TGMPA.
		"default_path" => "",                      // Default absolute path to bundled plugins.
		"menu"         => "tgmpa-install-plugins", // Menu slug.
		"parent_slug"  => "themes.php",            // Parent menu slug.
		"capability"   => "edit_theme_options",    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		"has_notices"  => true,                    // Show admin notices or not.
		"dismissable"  => true,                    // If false, a user cannot dismiss the nag message.
		"dismiss_msg"  => "",                      // If 'dismissable' is false, this message will be output at top of nag.
		"is_automatic" => false,                   // Automatically activate plugins after installation or not.
		"message"      => "",                      // Message to output right before the plugins table.

		/*
		'strings'      => array(
			'page_title'                      => __( 'Install Required Plugins', 'blank-theme' ),
			'menu_title'                      => __( 'Install Plugins', 'blank-theme' ),
			/* translators: %s: plugin name. * /
			'installing'                      => __( 'Installing Plugin: %s', 'blank-theme' ),
			/* translators: %s: plugin name. * /
			'updating'                        => __( 'Updating Plugin: %s', 'blank-theme' ),
			'oops'                            => __( 'Something went wrong with the plugin API.', 'blank-theme' ),
			'notice_can_install_required'     => _n_noop(
				/* translators: 1: plugin name(s). * /
				'This theme requires the following plugin: %1$s.',
				'This theme requires the following plugins: %1$s.',
				'blank-theme'
			),
			'notice_can_install_recommended'  => _n_noop(
				/* translators: 1: plugin name(s). * /
				'This theme recommends the following plugin: %1$s.',
				'This theme recommends the following plugins: %1$s.',
				'blank-theme'
			),
			'notice_ask_to_update'            => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
				'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
				'blank-theme'
			),
			'notice_ask_to_update_maybe'      => _n_noop(
				/* translators: 1: plugin name(s). * /
				'There is an update available for: %1$s.',
				'There are updates available for the following plugins: %1$s.',
				'blank-theme'
			),
			'notice_can_activate_required'    => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following required plugin is currently inactive: %1$s.',
				'The following required plugins are currently inactive: %1$s.',
				'blank-theme'
			),
			'notice_can_activate_recommended' => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following recommended plugin is currently inactive: %1$s.',
				'The following recommended plugins are currently inactive: %1$s.',
				'blank-theme'
			),
			'install_link'                    => _n_noop(
				'Begin installing plugin',
				'Begin installing plugins',
				'blank-theme'
			),
			'update_link' 					  => _n_noop(
				'Begin updating plugin',
				'Begin updating plugins',
				'blank-theme'
			),
			'activate_link'                   => _n_noop(
				'Begin activating plugin',
				'Begin activating plugins',
				'blank-theme'
			),
			'return'                          => __( 'Return to Required Plugins Installer', 'blank-theme' ),
			'plugin_activated'                => __( 'Plugin activated successfully.', 'blank-theme' ),
			'activated_successfully'          => __( 'The following plugin was activated successfully:', 'blank-theme' ),
			/* translators: 1: plugin name. * /
			'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'blank-theme' ),
			/* translators: 1: plugin name. * /
			'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'blank-theme' ),
			/* translators: 1: dashboard link. * /
			'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'blank-theme' ),
			'dismiss'                         => __( 'Dismiss this notice', 'blank-theme' ),
			'notice_cannot_install_activate'  => __( 'There are one or more required or recommended plugins to install, update or activate.', 'blank-theme' ),
			'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'blank-theme' ),

			'nag_type'                        => '', // Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
		),
		*/
	);
	tgmpa($plugins, $config);
}


// CUSTOM MENUS
/*
function custom_menus() {
	register_nav_menus(
		array(
			"primary-menu" => __("Primary Menu"),
			"secondary-menu" => __("Secondary Menu"),
			"footer-menu" => __("Footer Menu")
		)
	);
}
add_action("init", "custom_menus");
*/

// ADD CUSTOM POST TYPE IF NEEDED

function custom_post_type_newsletter() {
	$labels = array(
		"name"               => _x("Newsletters", "post type general name"),
		"singular_name"      => _x("Newsletters", "post type singular name"),
		"add_new"            => _x("Add New", "Newsletter"),
		"add_new_item"       => __("Add New Newsletter"),
		"edit_item"          => __("Edit Newsletter"),
		"new_item"           => __("New Newsletter"),
		"all_items"          => __("All Newsletters"),
		"view_item"          => __("View Newsletter"),
		"search_items"       => __("Search Newsletters"),
		"not_found"          => __("No Newsletters found"),
		"not_found_in_trash" => __("No Newsletters found in the Trash"),
		"parent_item_colon"  => "",
		"menu_name"          => "Newsletters"
	);

	$args = array(
		"labels"        => $labels,
		"description"   => "Holds our Newsletters and Newsletter specific data",
		"public"        => true,
		"menu_position" => 5,
		"supports"      => array( "title", "editor", "thumbnail", "post-formats" ,"custom-fields"),
		"has_archive"   => true,
	);
	register_post_type("cpt_newsletter", $args);
}
add_action("init", "custom_post_type_newsletter");


// ADD CUSTOM TAXONOMY IF NEEDED
/*
function custom_taxonomy_type() {
	$labels = array(
		"name"              => _x("Event Type", "taxonomy general name"),
		"singular_name"     => _x("Event Type", "taxonomy singular name"),
		"search_items"      => __("Search Event Types"),
		"all_items"         => __("All Event Types"),
		"parent_item"       => __("Parent Event Type"),
		"parent_item_colon" => __("Parent Event Type:"),
		"edit_item"         => __("Edit Event Type"),
		"update_item"       => __("Update Event Type"),
		"add_new_item"      => __("Add New Event Type"),
		"new_item_name"     => __("New Event Type"),
		"menu_name"         => __("Event Type"),
	);

	$args = array(
		"hierarchical"      => true,
		"labels"            => $labels,
		"show_ui"           => true,
		"show_admin_column" => true,
		"query_var"         => true,
		"rewrite"           => array("slug" => "event-type"),
	);

	register_taxonomy("event-type", array("conjunctures"), $args);
}
add_action("init", "custom_taxonomy_type", 0);
*/

// IF TAG SEARCH NEEDED
/*
function atom_search_where($where) {
	global $wpdb;
	if (is_search())
		$where .= "OR (t.name LIKE '%".get_search_query()."%' AND {$wpdb->posts}.post_status = 'publish')";
	return $where;
}
add_filter("posts_where", "atom_search_where");

function atom_search_join($join) {
	global $wpdb;
	if (is_search())
		$join .= "LEFT JOIN {$wpdb->term_relationships} tr ON {$wpdb->posts}.ID = tr.object_id LEFT JOIN {$wpdb->term_taxonomy} tt ON tt.term_taxonomy_id=tr.term_taxonomy_id LEFT JOIN {$wpdb->terms} t ON t.term_id = tt.term_id";
	return $join;
}
add_filter("posts_join", "atom_search_join");

function atom_search_groupby($groupby) {
	global $wpdb;

	// we need to group on post ID
	$groupby_id = "{$wpdb->posts}.ID";
	if(!is_search() || strpos($groupby, $groupby_id) !== false) return $groupby;

	// groupby was empty, use ours
	if(!strlen(trim($groupby))) return $groupby_id;

	// wasn't empty, append ours
	return $groupby.", ".$groupby_id;
}
add_filter("posts_groupby", "atom_search_groupby");
*/

//disable Gutenberg

add_filter( 'use_block_editor_for_post', '__return_false' );

// CUSTOM MENUS

function custom_menus() {
    register_nav_menus(
        array(
            "primary-menu" => __("Primary Menu"),
            "secondary-menu" => __("Secondary Menu"),
            "footer-menu1" => __("Footer Menu1"),
            "footer-menu2" => __("Footer Menu2"),
        )
    );
}
add_action("init", "custom_menus");


function img_responsive($content){
    return str_replace('<img class="','<img class="img-responsive ',$content);
}
add_filter('the_content','img_responsive');