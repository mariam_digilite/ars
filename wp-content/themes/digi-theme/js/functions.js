var $ = jQuery;
var header = document.getElementById("stickyMenu");
var sticky = header.offsetTop;
if($('.blog-slider').length > 0){
	var swiper = new Swiper('.blog-slider', {
		spaceBetween: 30,
		effect: 'fade',
		loop: true,
		mousewheel: {
			invert: false,
		},
		// autoHeight: true,
		pagination: {
			el: '.blog-slider__pagination',
			clickable: true,
		}
	});
}


var map = document.getElementById('map');

$(document).ready(function() {
	if(map !== null && map !== ''){
		initMap();
	}

	$('.menu-item-has-children > a').on('click', function(e){
		e.preventDefault();
	});

	$('.first-button').on('click', function () {

		$('.animated-icon1').toggleClass('open');
	});

});

$(window).load(function() {
	var viewHeight = $(window).height();
	var bodyHeight = $("body").outerHeight();

	if(bodyHeight <= viewHeight) {
		$("body").css("min-height", viewHeight);
		$("footer").addClass("absolute-bottom");
	}
});

// UNCOMMENT FOLLOWING CODE IF YOU WANT TO PREVENT RIGHT CLICK OR INSPECT ELEMENT
// $(document).on("contextmenu", function() {
// 	return false; //Prevent right click
// });

// $(document).keydown(function(event) {
// 	if(event.keyCode == 123) {
// 		return false; //Prevent from f12
// 	} else if(event.ctrlKey && event.shiftKey && event.keyCode == 73) {
// 		return false; //Prevent from ctrl+shift+i
// 	}
// });

window.onscroll = function() {
	myFunction();

	$(".donate-section").addClass('scrolled');
	if ((window.innerHeight + Math.ceil(window.pageYOffset)) >= document.body.offsetHeight) {
		$(".donate-section").removeClass('scrolled');
	}


};

function myFunction() {
	if (window.pageYOffset > sticky) {
		header.classList.add("sticky");
	} else {
		header.classList.remove("sticky");
	}
}
