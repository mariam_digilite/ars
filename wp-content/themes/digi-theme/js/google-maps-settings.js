function initMap() {

    var latLang = {lat:  43.7693295, lng: -79.3262892};
    var map = new google.maps.Map(
        document.getElementById('map'), {
            zoom: 15,
            center: latLang,
        });

    var baseUrl = window.location.origin + "/ars-canada/wp-content/themes/digi-theme/img/";
    var marker = new google.maps.Marker({
        position: latLang,
        map: map,
    });
}