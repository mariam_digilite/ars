<?php get_header(); ?>
    <?php if ( have_posts() ) : ?>
        <div class="main-content contact-us-page">
            <div class="container">
                <?php while ( have_posts() ) : the_post(); ?>
                    <h1 class="p-title playfair-font"><?= get_the_title(); ?></h1>
                    <div class="page-content"><?= get_the_content(); ?></div>
                    <div class="contact-page"><?php get_template_part("templates/contact-info"); ?></div>
                <?php endwhile; ?>
                <div class="contact-form">
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4">
                            <h2 class="playfair-font">Get in Touch</h2>
                            <?php echo do_shortcode('[formidable id=7]'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div id="map" class="maps"></div>
    <?php endif; ?>
<?php get_footer(); ?>