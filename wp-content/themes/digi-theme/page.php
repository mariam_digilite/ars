<?php get_header(); ?>
    <?php if ( have_posts() ) : ?>
        <div class="simple-page">
            <?php while ( have_posts() ) : the_post(); ?>
                <h1 class="p-title playfair-font"><?= get_the_title(); ?></h1>

                <?php if(!empty(get_the_content())): ?>
                    <div class="container">
                        <div class="row">
                            <div class="hero-bkg-animated" style="background: url(<?php if(!empty($bg = get_field('background_image'))): echo $bg; endif;?>) no-repeat, center center fixed"></div>
                            <div class="col-sm-6 col-sm-offset-3">
                                <div class="content-sections">
                                    <?= get_the_content(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php get_template_part("content"); ?>

            <?php endwhile; ?>
        </div>
    <?php endif; ?>
<?php get_footer(); ?>


