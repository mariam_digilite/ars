<?php get_header(); ?>
    <div class="simple-page">
        <div class='container'>
            <div class='row'>
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="search-content">
                        <?php
                        if(have_posts()):
                            $counter = 1;
                            while(have_posts()):
                                the_post(); ?>
                                <div class='search item'>
                                    <a href="<?php the_permalink(); ?>">
                                        <div class='animate'><?php the_title(); ?></div>
                                    </a>
                                </div>
                                <?php
                                if ($counter % 4 == 0) {
                                    echo "<div class=\"clearfix visible-sm\"></div>";
                                }
                                $counter++;
                            endwhile;
                        else:
                            echo "<h3 class='text-center'>Nothing Found</h3>";
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>