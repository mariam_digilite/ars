<?php get_header(); ?>
<?php if ( have_posts() ) : ?>
    <div class="simple-page">
        <?php while ( have_posts() ) : the_post(); ?>
            <div class="container">
                <div class="row">
                    <h1 class="p-title playfair-font"><?= get_the_title(); ?></h1>
                    <div class="col-sm-8 col-sm-offset-2">
                        <div class="single-page-content">
                            <?php if(!empty(get_the_post_thumbnail())): ?>
                                <div class="post-thumbnail">
                                    <?= get_the_post_thumbnail(get_the_ID(), 'medium'); ?>
                                </div>
                            <?php endif; ?>
                            <div class="page-content"><?= get_the_content(); ?></div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
    </div>
<?php endif; ?>
<?php get_footer(); ?>