<?php if(have_rows("social_links", "option")): ?>
    <ul class="socila-icons">
        <?php while(have_rows("social_links", "option")): the_row(); ?>
            <?php if(get_sub_field("link")): ?>
                <li>
                    <a href="<?php the_sub_field("link"); ?>" target="_blank" class="transition fa-icon">
                        <?php the_sub_field("icon"); ?>
                    </a>
                </li>
            <?php endif; ?>
        <?php endwhile; ?>
    </ul>
<?php endif; ?>

