<?php $contact_info = get_field('contact_info');
if(!empty($contact_info)): ?>
    <div class="flex-content">
        <?php foreach($contact_info as $info):
            $title = $info['contact_section_title'];
            $contact_person = $info['contact_person'];
            $telephone = $info['telephone'];
            $fax = $info['fax'];
            $email_address = $info['email_address'];
            $mailing_address = $info['mailing_address']; ?>
            <div class="flex-box">
                <?php if(!empty($title)): ?>
                    <p><?= $title; ?></p>
                <?php endif; ?>
                <?php if(!empty($contact_person)): ?>
                    <p>Contact: <?= $contact_person; ?></p>
                <?php endif; ?>
                <?php if(!empty($telephone)): ?>
                    <p>Telephone: <a href="tel:<?= $telephone; ?>"><?= $telephone; ?></a></p>
                <?php endif; ?>
                <?php if(!empty($fax)): ?>
                    <p>Fax: <a href="fax:<?= $fax; ?>"><?= $fax; ?></a></p>
                <?php endif; ?>
                <?php if(!empty($email_address)): ?>
                    <p>Email: <a class="email" href="mailto:<?= $email_address; ?>"><?= $email_address; ?></a></p>
                <?php endif; ?>
                <?php if(!empty($mailing_address)): ?>
                    <p><?= $mailing_address; ?></p>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>