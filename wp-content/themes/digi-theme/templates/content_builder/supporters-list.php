<section class="content-sections supporter-section">

    <div class="container">

        <?php if($supporters = get_sub_field("supporters")): ?>

            <?php if($title = get_sub_field("supporters_section_title")): ?>
                <h4 class="supporter-section-title"><?= $title; ?></h4>
            <?php endif; ?>
            <div class="supporter-list flex-content">
                <?php foreach($supporters as $supporter):
                    $bg_image = $supporter['supporter_image'];
                ?>
                    <div class="supporter">
                        <div class="supporter-img" style="background: url(<?= $bg_image; ?>) no-repeat top / cover;"></div>
                    </div>
                <?php endforeach;?>
            </div>

        <?php endif; ?>

    </div>

</section>