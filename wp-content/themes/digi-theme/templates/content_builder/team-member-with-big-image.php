<section class="content-sections team-member-with-big-img-section">
    <div class="container">
        <?php if(get_sub_field("team_member_section_title1")):?>
            <h2 class="playfair-font text-center"><?= get_sub_field("team_member_section_title1"); ?></h2>
        <?php endif; ?>
        <?php if(get_sub_field("team_members1")):
            $team_members = get_sub_field('team_members1'); ?>
            <div class="row">
                <?php foreach($team_members as $team_member): ?>
                    <div class="col-sm-4 col-sm-offset-2 col-xs-12 big-profile-picture">
                        <div class="img-with-line-grad">
                            <div class="profile-picture" style="background:url('<?= $team_member['person_profile_image1']; ?>') no-repeat top / cover;"></div>
                        </div>
                        <h3 class="person-name text-center"><?= $team_member['person_name1']; ?></h3>
                        <p class="position text-center"><?= $team_member['position1']; ?></p>
                    </div>
                    <div class="col-sm-4 col-xs-12 big-profile-picture">
                        <div class="img-with-line-grad">
                            <div class="profile-picture" style="background:url('<?= $team_member['person_profile_image1']; ?>') no-repeat top / cover;"></div>
                        </div>
                        <h3 class="person-name text-center"><?= $team_member['person_name1']; ?></h3>
                        <p class="position text-center"><?= $team_member['position1']; ?></p>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</section>