<section class="content-sections team-member-section">
    <div class="container">
        <?php if(get_sub_field("team_member_section_title")):?>
            <h2 class="playfair-font text-center"><?= get_sub_field("team_member_section_title"); ?></h2>
        <?php endif; ?>
        <?php if(get_sub_field("team_members")):
            $team_members = get_sub_field('team_members'); ?>
                <?php $i = 0; foreach ($team_members as $team_member): $i++; ?>
                <div class="directors-board">
                    <div class="row flex-center-content">
                        <?php if($i%2==0): ?>
                        <div class="col-md-3 col-md-push-7 col-sm-6 col-sm-push-6 col-xs-12">
                            <div class="img-with-line-grad">
                                <div class="profile-picture" style="background:url('<?= $team_member['person_profile_image']; ?>") no-repeat top / cover, linear-gradient(90deg, #1A2980 0%, #26D0CE 100%);"></div>
                            </div>
                        </div>
                        <div class="col-md-5 col-md-offset-2 col-md-pull-3 col-sm-6 col-sm-pull-6 col-xs-12">
                            <h3 class="person-name playfair-font"><?= $team_member['person_name']; ?></h3>
                            <p class="position"><?= $team_member['position']; ?></p>
                            <div class="about-person"><?= $team_member['about_person']; ?></div>
                        </div>
                    <div class="col-sm-2 hidden-sm"></div>
                    <?php else: ?>
                        <div class="col-md-3 col-md-offset-2 col-sm-6 col-xs-12">
                            <div class="img-with-line-grad">
                                <div class="profile-picture" style="background:url('<?= $team_member['person_profile_image']; ?>")  no-repeat top / cover;"></div>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-6 col-xs-12">
                            <h3 class="person-name"><?= $team_member['person_name']; ?></h3>
                            <p class="position"><?= $team_member['position']; ?></p>
                            <div class="about-person"><?= $team_member['about_person']; ?></div>
                        </div>
                        <div class="col-md-2 hidden-sm"></div>
                    <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</section>