<section class="content-sections">

    <div class="container">

        <?php if ($files = get_sub_field("files")): ?>

            <?php if($title = get_sub_field("upload_file_section_title")): echo "<h2 class='form-title playfair-font'>{$title}</h2>"; endif; ?>

            <?php if($sub_title = get_sub_field("upload_file_section_subtitle")): echo "<h3 class='subtitle playfair-font'>{$sub_title}</h3>"; endif; ?>

            <div class="file-container flex-content">

                <?php foreach($files as $file): ?>
                    <div class="file-box">
                        <a href="<?= $file['pdf_file']; ?>" class="absolute" download></a>
                        <img class="file-icon" src="<?php echo get_template_directory_uri(); ?>/img/pdf_inactive.svg" />
                        <h4 class="file-title"><?= $file['file_title']; ?></h4>
                    </div>
                <?php endforeach; ?>

            </div>

        <?php endif; ?>

    </div>

</section>